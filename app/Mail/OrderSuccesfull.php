<?php

namespace App\Mail;

use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderSuccesfull extends Mailable
{
    use Queueable, SerializesModels;


    public $subject = "Comfirmación de pedido";
    public $order;
    public $items;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->items = json_decode($order->content);

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $items = $this->items;

        $user = User::where('id', auth()->user()->id)->first();
        return $this->markdown('mail.order-successfull', compact('user'));
    }
}
