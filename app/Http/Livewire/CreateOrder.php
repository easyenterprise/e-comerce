<?php

namespace App\Http\Livewire;


use App\Models\City;
use App\Models\Department;
use App\Models\District;
use App\Models\Order;

use Gloudemans\Shoppingcart\Facades\Cart;

use Livewire\Component;

class CreateOrder extends Component
{
    public $envio_type = 1;
    public $contact, $phone, $address, $references, $shipping_cost = 0;
    public $departments, $cities = [], $districts = [];
    public $department_id = "", $city_id = "", $district_id = "";
    public $subtotal;

    public $rules = [
        'contact' => 'required|string',
        'phone' => 'required|digits:10',
        'envio_type' => 'required'
    ];

    public $messages = [
        'contact.required' => 'El campo de nombre es requerido',
        'contact.string' => 'El campo de nombre solo permite letras',
        'phone.required' => 'El campo de telefono es requerido',
        'phone.digits' => 'El campo de telefono debe ser de 10 digitos',
    ];

    public function mount()
    {
        $this->departments = Department::all();

        foreach (Cart::content() as $item) {
            $this->subtotal += $item->price * $item->qty;
        }
    }

    public function updatedEnvioType($value)
    {
        if ($value == 1) {
            $this->resetValidation([
                'department_id',
                'city_id',
                'district_id',
                'address',
                'references'
            ]);
        }
    }

    public function updatedDepartmentId($value)
    {
        $this->cities = City::where('department_id', $value)->get();
        $this->reset(['city_id', 'district_id']);
    }

    public function updatedCityId($value)
    {
        $city = City::find($value);

        $this->shipping_cost = $city->cost;

        $this->districts = District::where('city_id', $value)->get();

        $this->reset('district_id');
    }

    public function create_order()
    {
        $rules =  $this->rules;
        $messages = $this->messages;

        //Agregando reglas de validacion cuando se ejecute el metodo create_order al hacer click en el boton de la vista
        if ($this->envio_type == 2) {
            $rules['department_id'] = 'required';
            $rules['city_id'] = 'required';
            $rules['district_id'] = 'required';
            $rules['address'] = 'required';
            $rules['references'] = 'required';
            $messages['district_id.required'] = 'Porfavor seleccione un distrito';
            $messages['city_id.required'] = 'Porfavor seleccione una ciudad';
            $messages['department_id.required'] = 'Porfavor seleccione un departamento';
            $messages['references.required'] = 'El campo de referencia es requerido';
            $messages['address.required'] = 'El campo de direccion es requerido';
        }
        $this->validate($rules, $messages);

        //Se crea un objeto de tipo orden para enviar los datos a la vista de pago
        $order = new Order();
        $order->user_id = auth()->user()->id;
        $order->contact = $this->contact;
        $order->phone = $this->phone;
        $order->envio_type = $this->envio_type;
        $order->shipping_cost = 0;
        $order->total = $this->shipping_cost + $this->subtotal;
        $order->content = Cart::content();

        if ($this->envio_type == 2) {
            $order->shipping_cost = $this->shipping_cost;
            $order->department_id = $this->department_id;
            $order->city_id = $this->city_id;
            $order->district_id =  $this->district_id;
            $order->address = $this->address;
            $order->references = $this->references;
        }
        
        $order->save();
        foreach (Cart::content() as $item) {
            //helper
            discount($item);
        }

        Cart::destroy();

        return redirect()->route('orders.payment', $order);
    }
    public function render()
    {
        return view('livewire.create-order');
    }
}
