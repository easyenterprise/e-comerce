<?php

namespace App\Http\Livewire\Admin;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\Subcategory;
use App\Models\User;
use Livewire\Component;

class TableroComponent extends Component
{
    public function render()
    {
        $users = User::all();
        $products = Product::all();
        $orders = Order::all();
        $categories = Category::all();
        $brands = Brand::all();
        $subcategories = Subcategory::all();
        return view('livewire.admin.tablero-component', compact('users', 'products', 'orders', 'categories','brands', 'subcategories'))->layout('layouts.admin');
    }
}
