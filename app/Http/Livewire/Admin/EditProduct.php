<?php

namespace App\Http\Livewire\Admin;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Illuminate\Support\Str;
use App\Models\OptionProduct as Pivot;

class EditProduct extends Component
{
    public $product, $categories, $subcategories, $brands, $slug;
    public $pointssell, $pointsbuy;
    public $promo = false;
    public $category_id;

    public $rules = [
        'category_id' => 'required',
        'product.subcategory_id' => 'required',
        'product.name' => 'required',
        'slug' => 'required|unique:products,slug',
        'product.description' => 'required',
        'product.brand_id' => 'required',
        'product.quantity' => 'nullable|numeric',
        'product.price' => 'nullable|numeric',
        'product.promo' => 'boolean',
        'product.points_sell' => 'numeric',
        'product.points_buy' => 'numeric',
    ];

    public $messages = [
        'category_id.required' => 'Porfavor seleccione una categoria',
        'product.subcategory_id.required' => 'Porfavor seleccione una subcategoria',
        'product.name.required' => 'Debe escribir un nombre',
        'product.slug.required' => 'El slug esta vacio, verifique el nombre',
        'product.description.required' => 'Debe proporcionar una descripcion del producto',
        'product.brand_id.required' => 'Porfavor seleccione una marca'
    ];

    protected $listeners=['refreshProduct', 'delete'];


    // Este metodo carga los datos obtenidos de la base en los forms
    public function mount(Product $product){
        $this->product = $product;
        $this->categories = Category::all();
        $this->category_id = $product->subcategory->category->id;
        $this->subcategories = Subcategory::where('category_id', $this->category_id)->get();
        $this->slug = $this->product->slug;
        $this->promo = $product->promo;
        $this->pointsbuy = $product->points_buy;
        $this->pointssell = $product->points_sell;
        $this->brands = Brand::whereHas('categories', function(Builder $query){
            $query->where('category_id', $this->category_id);
        })->get();
    }

    public function updatedCategoryId($value){
        $this->subcategories = Subcategory::where('category_id',$value)->get();
        $this->brands = Brand::whereHas('categories', function(Builder $query) use ($value){
            $query->where('category_id', $value);
        })->get();
        
        $this->product->subcategory_id = "";
        $this->product->brand_id = "";
    }

    public function refreshProduct(){
        $this->product = $this->product->fresh();
    }

    public function updatedProductName($value){
        $this->slug = Str::slug($value);
    }
    public function getSubcategoryProperty(){
        return Subcategory::find($this->product->subcategory_id);
    }
    public function save(){
        $rules = $this->rules;
        $messages = $this->messages;

        $rules['slug'] = 'required|unique:products,slug,' . $this->product->id;

        if ($this->product->subcategory_id) {
            if (!$this->subcategory->option) {
                $rules['product.quantity'] = 'required|numeric';
                $messages['product.quantity.required'] = 'Debe ingresar una cantidad';
                $rules['product.price'] = 'required|numeric';
                $messages['product.price.required'] = 'Debe ingresar un precio';
            }
        }

        $this->validate($rules,$messages);

        $this->product->slug = $this->slug;
        $this->product->promo = $this->promo;
        $this->product->points_buy = $this->pointsbuy;
        $this->product->points_sell = $this->pointssell;

        $this->product->save();

        $this->emit('saved');
    }


    public function deleteImage(Image $image){
        Storage::delete([$image->url]);
        $image->delete();
        $this->product = $this->product->fresh();
    }

    public function delete(){
        $images = $this->product->images;
        foreach ($images as $image) {
            Storage::delete($image->url);
            $image->delete();
        }
        $pivots = Pivot::where('product_id', $this->product->id)->get();
        foreach ($pivots as $pivot) {
            $pivot->delete();
        }
        $this->product->delete();

        session()->flash('status', 'Producto eliminado satisfactoriamente!');
        return redirect()->route('admin.products.index');
    }

    public function render()
    {
        return view('livewire.admin.edit-product')->layout('layouts.admin');
    }
}
