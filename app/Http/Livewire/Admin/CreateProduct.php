<?php

namespace App\Http\Livewire\Admin;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Illuminate\Support\Str;

class CreateProduct extends Component
{
    public $categories, $subcategories = [], $brands = [];
    // Variables sincronizadas con la vista en cada una con wire:model
    public $category_id = "", $subcategory_id="", $brand_id = "";
    public $name, $slug, $description, $price, $quantity;

    public $rules = [
        'category_id' => 'required',
        'subcategory_id' => 'required',
        'name' => 'required',
        'slug' => 'required|unique:products',
        'description' => 'required',
        'brand_id' => 'required',
        'price' => 'required'
    ];
    public $messages = [
        'category_id.required' => 'Porfavor seleccione una categoria',
        'subcategory_id.required' => 'Porfavor seleccione una subcategoria',
        'name.required' => 'Debe escribir un nombre',
        'slug.required' => 'El slug esta vacio, verifique el nombre',
        'description.required' => 'Debe proporcionar una descripcion del producto',
        'brand_id.required' => 'Porfavor seleccione una marca',
        'price.required' => 'Debe ingresar un precio'
    ];

    // Metodo que actualiza los valores de cada option de la vista y los resetea
    public function updatedCategoryId($value){
        $this->subcategories = Subcategory::where('category_id',$value)->get();
        $this->brands = Brand::whereHas('categories', function(Builder $query) use ($value){
            $query->where('category_id', $value);
        })->get();
        $this->reset(['subcategory_id', 'brand_id']);
    }

    // Metodo para actualizar el input de Slug cuando se escribe el nombre
    public function updatedName($value){
        $this->slug = Str::slug($value);
    }

    public function getSubcategoryProperty(){
        return Subcategory::find($this->subcategory_id);
    }

    // Este metodo carga al momento de ingresar a la pagina
    public function mount(){
        $this->categories = Category::all();
    }

    public function save(){
        $rules =  $this->rules;
        $messages = $this->messages;

        if ($this->subcategory_id) {
            if (!$this->subcategory->option) {
                $rules['quantity'] = 'required';
                $messages['quantity.required'] = 'Debe ingresar una cantidad';
            }
        }
        $this->validate($rules,$messages);

        $product = new Product();
        $product->name = $this->name;
        $product->slug = $this->slug;
        $product->description = $this->description;
        $product->subcategory_id = $this->subcategory_id;
        $product->brand_id = $this->brand_id;
        $product->price = $this->price;
        if ($this->subcategory_id) {
            if (!$this->subcategory->option) {
                $product->quantity = $this->quantity;
            }
        }
        $product->save();
        return redirect()->route('admin.products.edit', $product);
    }
    
    public function render()
    {
        return view('livewire.admin.create-product')->layout('layouts.admin');
    }
}
