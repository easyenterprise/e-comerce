<?php

namespace App\Http\Livewire\Admin;

use App\Models\Option;
use App\Models\OptionProduct as Pivot;
use Livewire\Component;

class OptionProduct extends Component
{
    public $product, $options, $option_id, $quantity,$price, $open = false;
    public $pivot_quantity, $pivot, $pivot_price;
    public $pivot_option_id = " ";
    protected $listeners =['delete'];

    public $rules = [
        'option_id' => 'required',
        'quantity' => 'required|numeric',
        'price' => 'required|numeric'
    ];

    public $messages = [
        'option_id.required' => 'Porfavor selecione una opción',
        'quantity.required' => 'El campo cantidad es requerido',
        'price.required' => 'El campo precio es requerido'
    ];

    public function mount(){
        $this->options = Option::all(); 
    }
    public function save(){
        $this->validate();

        // $this->product->options()->attach([
        //     $this->option_id => [
        //         'quantity' => $this->quantity,
        //         'price' => $this->price
        //     ]
        // ]);

        $pivot = Pivot::where('option_id', $this->option_id)
                    ->where('product_id', $this->product->id)
                    ->first();

        if ($pivot) {

            $pivot->quantity = $pivot->quantity + $this->quantity;
            $pivot->save();

        } else {
            
            $this->product->options()->attach([
                $this->option_id => [
                    'quantity' => $this->quantity,
                    'price' => $this->price
                ]
            ]);
            
        }


        $this->reset(['option_id', 'quantity','price']);

        $this->emit('saved');

        $this->product = $this->product->fresh();
    }

    public function edit(Pivot $pivot){
        $this->open = true;
        $this->pivot = $pivot;
        $this->pivot_option_id = $pivot->option_id;
        $this->pivot_quantity = $pivot->quantity;
        $this->pivot_price = $pivot->price;
    }

    public function update(){
        $this->pivot->option_id = $this->pivot_option_id;
        $this->pivot->quantity = $this->pivot_quantity;
        $this->pivot->price = $this->pivot_price;

        $this->pivot->save();
        $this->product = $this->product->fresh();
        $this->open = false;
    }

    public function delete(Pivot $pivot){
        $pivot->delete();
        $this->product = $this->product->fresh();
    }

    // public function deletePivots(){
    //     $pivots = Pivot::where('product_id', $this->product->id)->get();
    //     foreach ($pivots as $pivot) {
    //         $pivot->delete();
    //     }
    // }

    public function render()
    {

        $product_options = $this->product->options;
        return view('livewire.admin.option-product',compact('product_options'));
    }
}
