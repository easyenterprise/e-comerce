<?php

namespace App\Http\Livewire\Admin;

use App\Models\Image;
use App\Models\Product;
use App\Models\OptionProduct as Pivot;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class ShowProducts extends Component
{
    use WithPagination;

    public $search;
    protected $listeners=['deleteProduct'];
    public function updatingSearch(){
        $this->resetPage();
    }
    
    public function deleteProduct($product){
        $images = Image::where('imageable_id', $product)->get();
        foreach ($images as $image) {
            Storage::delete($image->url);
            $image->delete();
        }
        $pivots = Pivot::where('product_id', $product)->get();
        foreach ($pivots as $pivot) {
            $pivot->delete();
        }
        DB::table('products')->whereId($product)->delete();

        session()->flash('status', 'Producto eliminado satisfactoriamente!');
        
    }

    public function render()
    {

        $products = Product::where('name', 'LIKE','%' . $this->search. '%')->paginate(10);
        return view('livewire.admin.show-products' , compact('products'))->layout('layouts.admin');
    }

}
