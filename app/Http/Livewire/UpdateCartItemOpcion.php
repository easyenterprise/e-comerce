<?php

namespace App\Http\Livewire;

use App\Models\Option;
use Livewire\Component;
use Gloudemans\Shoppingcart\Facades\Cart;

class UpdateCartItemOpcion extends Component
{
    public $rowId, $qty, $quantity, $price_option;

    public function mount(){
        $item = Cart::get($this->rowId);
        $this->qty = $item->qty;

        $option = Option::where('name', $item->options->option)->first();

        $this->quantity = qty_available($item->id, $option->id);
        $this->price_option = price($item->id, $option->id);
    }
    
    public function decrement(){
        $this->qty = $this->qty - 1;
        Cart::update($this->rowId, $this->qty);
        $this->emit('render');
    }
    public function increment(){
        $this->qty = $this->qty + 1;
        Cart::update($this->rowId, $this->qty);
        $this->emit('render');
    }
    
    public function render()
    {
        return view('livewire.update-cart-item-opcion');
    }
}
