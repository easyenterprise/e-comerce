<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Storage;

class AddCartItemOption extends Component
{
    public $product, $options;
    public $option_id = "";
    public $opciones = [];

    public $qty = 1;
    public $quantity = 0;
    public $price_option = 0;
    
    public function mount(){
        $this->options = $this->product->options;
        $this->opciones['image'] = Storage::url($this->product->images->first()->url);
    }
    public function updatedOptionId($value){
        $option = $this->product->options->find($value);
        $this->quantity = qty_available($this->product->id, $option->id);
        $this->price_option = price($this->product->id, $option->id);
        $this->opciones['pointssell'] = $this->product->points_sell;
        $this->opciones['pointsbuy'] = $this->product->points_buy;
        $this->opciones['option'] = $option->name;
        $this->opciones['option_id'] = $option->id;
    }
    public function decrement(){
        $this->qty = $this->qty - 1;
    }
    public function increment(){
        $this->qty = $this->qty + 1;
    }
    public function addItem(){
        Cart::add(['id' => $this->product->id, 
        'name' =>  $this->product->name, 
        'qty' =>  $this->qty, 
        'price' =>  $this->price_option, 
        'weight' => 550,
        'options' => $this->opciones
    ]);

    $this->quantity = qty_available($this->product->id, $this->option_id);
    $this->price_option = price($this->product->id, $this->option_id);

    $this->reset('qty');

    $this->emitTo('dropdown-cart', 'render');
    $this->emitTo('cart-mobil', 'render');
    }
    public function render()
    {
        return view('livewire.add-cart-item-option');
    }
}
