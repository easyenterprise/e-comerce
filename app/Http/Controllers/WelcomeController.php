<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Image;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class WelcomeController extends Controller
{
    public function __invoke()
    {

        // REVISAR METODO CARBON
        $date = CarbonInterval::createFromFormat('H:i:s', '00:5:00');
        $restante = $date;
        if (auth()->user()) {
            $pendiente = Order::where('status' , 1)->where('user_id', auth()->user()->id)->count();
            if ($pendiente) {
                $mensaje = "Ustes tiene $pendiente ordenes pendientes, " . "solo cuenta con $restante minutos para realizar el pago." ." <a class='font-bold' href='". route('orders.index') ."?status=1'>Ir a pagar</a>";
                session()->flash('flash.banner', $mensaje);
                session()->flash('flash.bannerStyle', 'warning');
            }
            
        }



        $promos = Product::where('promo', true)->with('images')->get();
        $categories = Category::all();        
        return view('welcome', compact('categories', 'promos'));
    }
}
