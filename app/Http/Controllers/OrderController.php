<?php

namespace App\Http\Controllers;

use App\Mail\OrderSuccesfull;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{


    protected $puntos = 0;

    public function index()
    {
        $orders = Order::query()->where('user_id', auth()->user()->id);

        if (request('status')) {
            $orders->where('status', request('status'));
        }

        $orders = $orders->get();

        $pendiente = Order::where('status', 1)->where('user_id', auth()->user()->id)->count();
        $recibido = Order::where('status', 2)->where('user_id', auth()->user()->id)->count();
        $enviado = Order::where('status', 3)->where('user_id', auth()->user()->id)->count();
        $entregado = Order::where('status', 4)->where('user_id', auth()->user()->id)->count();
        $anulado = Order::where('status', 5)->where('user_id', auth()->user()->id)->count();

        return view('orders.index', compact('orders', 'pendiente', 'recibido', 'enviado', 'entregado', 'anulado'));
    }

    public function show(Order $order)
    {

        $this->authorize('author', $order);

        $items = json_decode($order->content);

        return view('orders.show', compact('order', 'items'));
    }

    public function payment(Order $order)
    {

        $this->authorize('author', $order);
        $this->authorize('payment', $order);


        $items = json_decode($order->content);
        foreach ($items as $item) {
            $this->puntos += $item->options->pointssell * $item->qty;
            $puntos = $this->puntos;
        }
        return view('orders.payment', compact('order', 'items', 'puntos'));
    }


    // Metodo en modo desarrollo, cambiar este metodo a el controlador webhooks y borrar de este controlador
    public function pay(Order $order, Request $request)
    {

        //politica de accesso
        $this->authorize('author', $order);

        // $user = User::where('id', auth()->user()->id);

        //obtener el id del pago
        $payment_id = $request->get('payment_id');
        //consulta a la api de mercado libre para saber si el pago es aprobado,pendiente o rechazado
        $response = Http::get("https://api.mercadopago.com/v1/payments/$payment_id" . "?access_token=APP_USR-6210606681743598-091120-c67725b1454bbaf05efc2f5740d3e33b-822856015");
        $response = json_decode($response);
        $status =  $response->status;

        $user = User::where('id', auth()->user()->id)->first();


        if ($status == 'approved') {
            $order->status = 2;
            foreach ($user->getRoleNames() as $rol) {
                if ($rol == 'ClientePreferente') {
                    $items = json_decode($order->content);
                    foreach ($items as $item) {
                        $this->puntos += $item->options->pointssell * $item->qty;
                        $puntos = $this->puntos;
                    }
                    $puntos += $user->points;
                    DB::update('update users set points = ? where id = ?', [$puntos, auth()->user()->id]);
                }
            }
            $order->save();
            Mail::to($user->email)->send(new OrderSuccesfull($order));
        }
        //ESTE DEVULVE SI EL PAGO ES RECHAZADO O SI NO ES AUTORIZADO
        else if ($status == 'rejected'){
            return "PAGO RECHAZADO";

        }
        // ESTE DE VUELVE EL PAGO EN PROCESO
        else if($status == 'in_process'){
            return "Pago pendiente";
        }

        return $response;
        //return redirect()->route('orders.show', $order);
    }
}
