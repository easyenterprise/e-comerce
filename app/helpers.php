<?php

use App\Models\OptionProduct;
use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;


// QUANTITY
function quantity($product_id , $option_id = null){
    $product = Product::find($product_id);

    if ($option_id) {
        $quantity = $product->options->find($option_id)->pivot->quantity;
    }
    else{
        $quantity = $product->quantity;
    }

    return $quantity;
}

function qty_added($product_id , $option_id = null){
    $cart = Cart::content();

    $item = $cart->where('id', $product_id)
                ->where('options.option_id', $option_id)
                ->first();


    if ($item) {
        return $item->qty;
    }
    else{
        return 0;
    }
}

function qty_available($product_id , $option_id = null){
    return quantity($product_id , $option_id) - qty_added($product_id , $option_id);
}

function increase($item){
    $product = Product::find($item->id);
    $quantity = quantity($item->id , $item->options->option_id) + $item->qty;
    
    if ($item->options->option_id) {
        $product->options()->detach($item->options->option_id);
        $product->options()->attach([
            $item->options->option_id => ['quantity' => $quantity]
        ]);
    } else {
        $product->quantity = $quantity;
        $product->save();
    }
}


//PRICE

function price($product_id , $option_id = null){
    $product = Product::find($product_id);

    if ($option_id) {
        $price = $product->options->find($option_id)->pivot->price;
    }
    else{
        $price = $product->price;
    }

    return $price;
}


function discount($item){
    $product = Product::find($item->id);

    $qty_available = qty_available($item->id , $item->options->option_id);
    $price_available = price($item->id , $item->options->option_id);
    
    if ($item->options->option_id) {
        $product->options()->detach($item->options->option_id);
        $product->options()->attach([
            $item->options->option_id => ['quantity' => $qty_available , 'price' => $price_available]
        ]);
    } else {
        $product->quantity = $qty_available;
        $product->price = $price_available;
        $product->save();
    }
}