<?php

namespace App\Observers;
use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\Support\Facades\DB;

class ProductObserver
{
    // METODO QUE ESTA A LA ESCUCHA DE ALGUN CAMBIO AL MOMENTO DE EDITAR EL PRODUCTO 
    public function updated(Product $product){
        $subcategory_id = $product->subcategory_id;
        $subcategory = Subcategory::find($subcategory_id);

        if($subcategory->option){
            DB::update('update products set price = ? where id = ?', [null, $product->id]);
            DB::update('update products set quantity = ? where id = ?', [null, $product->id]);
            $product->fresh();
        }else{
            if ($product->options->count()) {
                $product->options()->detach();
            }
        }
        
    }
}
