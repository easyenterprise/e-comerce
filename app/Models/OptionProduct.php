<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OptionProduct extends Model
{
    use HasFactory;

    protected $table = "option_product";

    //relacion uno a muchos inversa
    public function option(){
        return $this->belongsTo(Option::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
