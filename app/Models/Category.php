<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug', 'image','icon'];

    // Relacion 1 a muchos
    public function subcategories(){
        return $this->hasMany(Subcategory::class);
    }

    // Rleacion muchos a muchos
    public function brands(){
        return $this->belongsToMany(Brand::class);
    }

    //

    public function products(){
        return $this->hasManyThrough(Product::class, Subcategory::class);
    }
    // Url Amigable
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
