<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'product_id',
        'rating',
        'comment'
    ];

    // Relacion uno a mucho inversa
    public function user(){
        return $this->belongsTo(User::class);
    }

    // Relacion uno a mucho inversa
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
