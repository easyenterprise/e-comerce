<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    const BORRADOR = 1;
    const PUBLICADO = 2;
    
    protected $guarded = ['id', 'created_at', 'updated_at'];

    //accsesores
    public function getStockAttribute(){
        if ($this->subcategory->option) {
            return OptionProduct::whereHas('product', function(Builder $query){
                $query->where('id', $this->id);
            })->sum('quantity');
        } else {
            return $this->quantity;
        }
        
    }

    // Relacion uno a muchos inversa
    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    // Relacion uno a muchos inversa
    public function subcategory(){
        return $this->belongsTo(Subcategory::class);
    }

    // Relacion muchos a muchos
    public function options(){
        return $this->belongsToMany(Option::class)->withPivot('quantity','id','price');
    }

    // Relacion uno a muchos polimorfica
    public function images(){
        return $this->morphMany(Image::class, "imageable");
    }

    //URLAMIGABLE
    public function getRouteKeyName()
    {
        return 'slug';
    }

    // Relacion uno a muchos
    public function reviews(){
        return $this->hasMany(Review::class);
    }
}
