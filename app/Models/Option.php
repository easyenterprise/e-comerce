<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use HasFactory;
    protected $fillable = ['name'];

    //relacion uno a muchos inversa

    public function products(){
        return $this->belongsToMany(Product::class);
    }
}
