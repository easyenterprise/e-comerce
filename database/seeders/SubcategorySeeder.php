<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Subcategory;

class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subcategories = [
            /* Perecederos */
            [
                'category_id' => 1,
                'name' => 'Frutas',
                'slug' => Str::slug('Frutas'),
                'option' => true,
            ],
            [
                'category_id' => 1,
                'name' => 'Verduras',
                'slug' => Str::slug('Verduras'),
            ],
            [
                'category_id' => 1,
                'name' => 'Arroz',
                'slug' => Str::slug('Arroz'),
            ],

            /* Productos enlatados*/

            [
                'category_id' => 2,
                'name' => 'Frijoles',
                'slug' => Str::slug('Frijoles'),
            ],
            [
                'category_id' => 2,
                'name' => 'Chiles',
                'slug' => Str::slug('Chiles'),
                'option' => true,
            ],
            [
                'category_id' => 2,
                'name' => 'Con conservador',
                'slug' => Str::slug('Con conservador'),
                'option' => true,
            ],

            /* Galletas */
            [
                'category_id' => 3,
                'name' => 'Chocolate',
                'slug' => Str::slug('Chocolate'),
            ],
            [
                'category_id' => 3,
                'name' => 'Avena',
                'slug' => Str::slug('Avena'),
                'option' => true,
            ],
            [
                'category_id' => 3,
                'name' => 'Sumplementarias',
                'slug' => Str::slug('Sumplementarias'),
            ],
            [
                'category_id' => 3,
                'name' => 'Soya',
                'slug' => Str::slug('Soya'),
                'option' => true,
            ],
            // Lacteos
            [
                'category_id' => 4,
                'name' => 'Leche Entera',
                'slug' => Str::slug('Leche Entera'),
                'option' => true,
            ],
            [
                'category_id' => 4,
                'name' => 'Soya',
                'slug' => Str::slug('Soya'),
                'option' => true,
            ],
            [
                'category_id' => 4,
                'name' => 'En Polvo',
                'slug' => Str::slug('En Polvo'),
                'option' => true,
            ],

            //Chatarra
            [
                'category_id' => 5,
                'name' => 'Sabritas',
                'slug' => Str::slug('Sabritas'),
                'option' => true,
            ],
            [
                'category_id' => 5,
                'name' => 'Refresco',
                'slug' => Str::slug('Refresco'),
                'option' => true,
            ],
            [
                'category_id' => 5,
                'name' => 'Gomitas',
                'slug' => Str::slug('Gomitas'),
                'option' => true,
            ]
        ];

        foreach ($subcategories as $subcategory) {
            SubCategory::factory(1)->create($subcategory);
        }
    }
}
