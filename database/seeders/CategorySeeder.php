<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Perecederos',
                'slug' => Str::slug('Perecederos'),
                'icon' => '<i class="fas fa-leaf"></i>'
            ],
            [
                'name' => 'Productos Enlatados',
                'slug' => Str::slug('Productos Enlatados'),
                'icon' => '<i class="fas fa-beer"></i>'
            ],
            [
                'name' => 'Galletas',
                'slug' => Str::slug('Galletas'),
                'icon' => '<i class="fas fa-cookie"></i>'
            ],
            [
                'name' => 'Lacteos',
                'slug' => Str::slug('Lacteos'),
                'icon' => '<i class="fas fa-cookie"></i>'
            ],
            [
                'name' => 'Chatarra',
                'slug' => Str::slug('Chatarra'),
                'icon' => '<i class="fas fa-cookie"></i>'
            ]
        ];

        foreach ($categories as $category) {
            $category = Category::factory(1)->create($category)->first();

            $brands = Brand::factory(4)->create();

            foreach ($brands as $brand) {
                $brand->categories()->attach($category->id);
            }
        }
    }
}
