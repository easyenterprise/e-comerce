<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Builder;

class OptionProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::whereHas('subcategory', function(Builder $query){
            $query->where('option',true);

        })->get();

        foreach ($products as $product) {
            $product->options()->attach([
                1 => [
                    'quantity' => 20,
                    'price' => 19.99
                ],
                2 => [
                    'quantity' => 30,
                    'price' => 49.99
                ],
                3 => [
                    'quantity' => 80,
                    'price' => 99.99
                ]
            ]);
        }
    }
}
