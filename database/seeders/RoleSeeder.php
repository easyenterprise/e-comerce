<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1 = Role::create(['name' => 'Administrador']);
        $role2 = Role::create(['name' => 'ClienteNormal']);
        $role3 = Role::create(['name' => 'ClientePreferente']);

        
        Permission::create(['name' => 'dashboard'])->syncRoles($role1,$role2,$role3);

        Permission::create(['name' => 'admin.home'])->assignRole($role1);
        Permission::create(['name' => 'admin.users'])->assignRole($role1);
        Permission::create(['name' => 'admin.users.index'])->assignRole($role1);
    }
}
