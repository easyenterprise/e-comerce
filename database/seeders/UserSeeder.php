<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Raul Pimentel',
            'email' => 'thezontrox@gmail.com',
            'password' => bcrypt('zontrox11'),
            'email_verified_at' => date_create(),
            'curp' => 'SAJHDKJASDHKAJS',
        ])->assignRole('Administrador');

        User::create([
            'name' => 'Madahi Ramirez',
            'email' => 'gmfor9000@gmail.com',
            'password' => bcrypt('zontrox11'),
            'email_verified_at' => date_create(),
            'curp' => 'SAJHDKJASDHKAJS',
        ])->assignRole('ClientePreferente');

        User::create([
            'name' => 'Juan Candido',
            'email' => 'juan@gmail.com',
            'password' => bcrypt('zontrox11'),
            'email_verified_at' => date_create(),
            'curp' => 'SAJHDKJASDHKAJS',
        ])->assignRole('ClienteNormal');
    }
}
