<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->sentence(2);
        $subcategory = Subcategory::all()->random();
        $category = $subcategory->category;
        $brands = $category->brands->random();

        if($subcategory->option){
            $quantity = null;
            $price = null;
        }
        else{
            $quantity = 10;
            $price = $this->faker->randomElement([19.99, 49.99, 99.99]);
        }

        return [
            'name' => $name,
            'slug' => Str::slug($name),
            'description' => $this->faker->text(),
            'price' => $price,
            'quantity' => $quantity,
            'points_sell' => $this->faker->randomNumber(2),
            'points_buy' => $this->faker->randomNumber(3),
            'status' => 2,
            'subcategory_id' => $subcategory->id,
            'brand_id' => $brands->id

        ];
    }
}
