<x-app-layout>
    @if ($promos->count() >= 1 && $promos->count() < 2)
        <div class="container my-8 flex justify-center">
                <div class="flex md:h-1/2 md:w-2/4">
                    @foreach ($promos as $product)
                    <li class="relative list-none rounded-lg hover:bg-black" data-thumb="{{ Storage::url($product->images->first()->url) }}">
                        <a href="{{ route('products.show', $product) }}"><img class="opacity-80 object-cover rounded-lg"
                                src="{{ Storage::url($product->images->first()->url) }}" /></a>
                        <div
                            class="absolute flex items-center justify-center right-0 top-1 h-10 bg-red-600 text-white font-semibold shadow-2xl mt-2 pr-4 pl-10 rounded-l-lg">
                            <p class="uppercase">en promoción</p>
                        </div>
                        <div
                            class="absolute flex items-center justify-center bottom-0 w-full h-16 bg-black bg-opacity-40 text-white font-bold text-xl rounded-b-lg">
                            <p class="uppercase">{{Str::limit($product->name, 15)}}</p>
                        </div>
                    </li>
                    @endforeach
                </div>
        </div>
    @endif
    
    @if ($promos->count() >= 2)
        <div class="container my-8">
            <div class="flexslider shadow-lg">
                <ul class="slides">
                    @foreach ($promos as $product)
                        <li class="relative" data-thumb="{{ Storage::url($product->images->first()->url) }}">
                            <a href="{{ route('products.show', $product) }}"><img class="opacity-80 object-cover"
                                    src="{{ Storage::url($product->images->first()->url) }}" /></a>
                            <div
                                class="absolute flex items-center justify-center right-0 top-0 h-10 bg-red-600 text-white font-semibold shadow-2xl mt-2 pr-4 pl-10 rounded-l-lg">
                                <p class="uppercase">en promoción</p>
                            </div>
                            <div
                                class="absolute flex items-center justify-center bottom-0 w-full h-16 bg-black bg-opacity-40 text-white font-bold text-xl">
                                <p class="uppercase">{{$product->name}}</p>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    <div class="container py-8">
        @foreach ($categories as $category)
            <section class="mb-6">
                <div class="flex items-center mb-2">
                    <h1 class="text-lg uppercase font-semibold text-gray-900">
                        {{ $category->name }}
                    </h1>
                    <a href="{{ route('categories.show', $category) }}"
                        class="text-indigo-700 hover:text-indigo-400 hover:underline ml-2 font-semibold "> Mostrar todos</a>
                </div>
                @livewire('category-products', ['category' => $category])
            </section>
        @endforeach
    </div>

    @push('script')
        <script>
            Livewire.on('glider', function(id) {
                new Glider(document.querySelector('.glider-' + id), {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    draggable: true,
                    dots: '.glider-' + id + '~ .dots',
                    arrows: {
                        prev: '.glider-' + id + '~ .glider-prev',
                        next: '.glider-' + id + '~ .glider-next'
                    },
                    responsive: [{
                            breakpoint: 640,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2,
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3,
                            }
                        },
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 4,
                                slidesToScroll: 4,
                            }
                        },
                        {
                            breakpoint: 1280,
                            settings: {
                                slidesToShow: 5,
                                slidesToScroll: 5,
                            }
                        },
                    ]
                });
            });
        </script>
    @endpush

    @push('script')
        <script>
            $(document).ready(function() {
                $('.flexslider').flexslider({
                    animation: "slide",
                    itemWidth: 640,
                    minItems: 1,
                    slideshowSpeed: 4000,
                    controlsContainer: $(".custom-controls-container"),
                    customDirectionNav: $(".custom-navigation a"),
                    itemMargin: 5,
                    before: function(slider) {
                        $('.flexslider').resize();
                    }
                });
            });
        </script>
    @endpush

</x-app-layout>
