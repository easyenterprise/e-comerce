<x-admin-layout>
    <div class="container py-12">
        @livewire('admin.create-category')
    </div>

    @push('script')
        <script>
            Livewire.on('deleteCategory', $categorySlug => {
                Swal.fire({
                    title: '¿Estas seguro?',
                    text: "Esta accion borrara todos los productos que se encuentren dentro de esta categoria!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Cancelar',
                    confirmButtonText: 'Si, Eliminar!'
                }).then((result) => {
                    if (result.isConfirmed) {

                        Livewire.emitTo('admin.create-category', 'delete', $categorySlug)
                        Swal.fire(
                            'Eliminado!',
                            'La categoria se ha aliminado de la base de datos!',
                            'success'
                        )
                    }
                })
            })
        </script>
    @endpush
</x-admin-layout>
