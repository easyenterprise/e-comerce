@component('mail::message')

<h1>Hola <b>{{ $user->name }}</b></h1>
<div class="bg-white shadow-lg rounded-lg border-gray-500">
<p>Su pago ha sido aprobado! a continuacion vera un resumen de su compra, puede visualizar el estado de su orden por medio del boto "ver estado de mi orden".</p>
</div>
<div>
<p style="margin-bottom: 0;">Persona que recibe: <strong>{{ $order->contact }}</strong></p>
<p style="margin-bottom: 0;">Telefono de contacto: <strong>{{ $order->phone }}</strong></p>
<p style="margin-bottom: 0;">Costo de envio: <strong>${{$order->shipping_cost}}</strong></p>
<p>Total pagado: <strong>${{ $order->total }}</strong></p>
</div>
<div>
<table class="table-auto w-full">
<thead>
<tr>
<th></th>
<th>Precio</th>
<th>Cantidad</th>
<th>Subtotal</th>
</tr>
</thead>
<tbody class="w-full">
@foreach ($items as $item)
<tr style="margin-bottom: 20px;">
<td>
<div class="mr-4">
<article>
<div>
<h1 class="mb-4" style="margin-bottom: 0;">{{ Str::limit($item->name, 15) }}</h1>
<div class="text-xs capitalize">
@isset($item->options->option)
Opcion: {{ $item->options->option }}
@endisset
</div>
</div>
</article>
</div>
</td>
<td class="text-center">
${{ $item->price }} MXN
</td>
<td class="text-center">
{{ $item->qty }}
</td>
<td class="text-center">
${{ $item->price * $item->qty }} MXN
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
<div>
</div>

@component('mail::button', ['url' => route('orders.show', $order->id),'color' => 'success'])
    Ver estado de mi orden
@endcomponent

Gracias por utilizar nuestro servicio.<br>
{{ config('app.name') }}
@endcomponent
