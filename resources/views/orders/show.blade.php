<x-app-layout>
    <div class="max-w-5xl mx-auto px-4 sm:px-6 lg:px-8 py-12">

        <div class="bg-white rounded-lg shadow-lg px-12 py-8 mb-6 flex items-center">

            @if ($order->status == 1)
            <div class="relative">
                <div class="{{ ($order->status >= 2 && $order->status != 5) ? 'bg-skyColor1-400' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-archive text-white"></i>
                </div>
                <div class="absolute -left-1.5 mt-0.5">
                    <p>Recibido</p>
                </div>
            </div>
            <div class="{{ ($order->status >= 3 && $order->status != 5) ? 'bg-yellow-400' : 'bg-gray-400'  }} h-1 flex-1 mx-2"></div>

            <div class="relative">
                <div class="{{ ($order->status >= 3 && $order->status != 5) ? 'bg-yellow-400' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-truck text-white"></i>
                </div>
                <div class="absolute -left-1 mt-0.5">
                    <p>Envíado</p>
                </div>
            </div>
            <div class="{{ ($order->status >= 4 && $order->status != 5) ? 'bg-green-500' : 'bg-gray-400'  }} h-1 flex-1 mx-2"></div>

            <div class="relative">
                <div class="{{ ($order->status >= 4 && $order->status != 5) ? 'bg-green-600' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-check text-white"></i>
                </div>
                <div class="absolute -left-2 mt-0.5">
                    <p>Entregado</p>
                </div>
            </div>

            @elseif($order->status == 2)
            <div class="relative">
                <div class="{{ ($order->status >= 2 && $order->status != 5) ? 'bg-green-600' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-archive text-white"></i>
                </div>
                <div class="absolute -left-1.5 mt-0.5">
                    <p>Recibido</p>
                </div>
            </div>
            <div class="{{ ($order->status >= 3 && $order->status != 5) ? 'bg-yellow-400' : 'bg-gray-400'  }} h-1 flex-1 mx-2"></div>

            <div class="relative">
                <div class="{{ ($order->status >= 3 && $order->status != 5) ? 'bg-yellow-400' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-truck text-white"></i>
                </div>
                <div class="absolute -left-1 mt-0.5">
                    <p>Envíado</p>
                </div>
            </div>
            <div class="{{ ($order->status >= 4 && $order->status != 5) ? 'bg-green-500' : 'bg-gray-400'  }} h-1 flex-1 mx-2"></div>

            <div class="relative">
                <div class="{{ ($order->status >= 4 && $order->status != 5) ? 'bg-green-600' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-check text-white"></i>
                </div>
                <div class="absolute -left-2 mt-0.5">
                    <p>Entregado</p>
                </div>
            </div>

            @elseif($order->status == 3)
            <div class="relative">
                <div class="{{ ($order->status >= 2 && $order->status != 5) ? 'bg-yellow-400' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-archive text-white"></i>
                </div>
                <div class="absolute -left-1.5 mt-0.5">
                    <p>Recibido</p>
                </div>
            </div>
            <div class="{{ ($order->status >= 3 && $order->status != 5) ? 'bg-yellow-400' : 'bg-gray-400'  }} h-1 flex-1 mx-2"></div>

            <div class="relative">
                <div class="{{ ($order->status >= 3 && $order->status != 5) ? 'bg-yellow-400' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-truck text-white"></i>
                </div>
                <div class="absolute -left-1 mt-0.5">
                    <p>Envíado</p>
                </div>
            </div>
            <div class="{{ ($order->status >= 4 && $order->status != 5) ? 'bg-green-500' : 'bg-gray-400'  }} h-1 flex-1 mx-2"></div>

            <div class="relative">
                <div class="{{ ($order->status >= 4 && $order->status != 5) ? 'bg-green-600' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-check text-white"></i>
                </div>
                <div class="absolute -left-2 mt-0.5">
                    <p>Entregado</p>
                </div>
            </div>

            @elseif($order->status == 4)
            <div class="relative">
                <div class="{{ ($order->status >= 2 && $order->status != 5) ? 'bg-green-600' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-archive text-white"></i>
                </div>
                <div class="absolute -left-1.5 mt-0.5">
                    <p>Recibido</p>
                </div>
            </div>
            <div class="{{ ($order->status >= 3 && $order->status != 5) ? 'bg-green-500' : 'bg-gray-400'  }} h-1 flex-1 mx-2"></div>

            <div class="relative">
                <div class="{{ ($order->status >= 3 && $order->status != 5) ? 'bg-green-600' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-truck text-white"></i>
                </div>
                <div class="absolute -left-1 mt-0.5">
                    <p>Envíado</p>
                </div>
            </div>
            <div class="{{ ($order->status >= 4 && $order->status != 5) ? 'bg-green-500' : 'bg-gray-400'  }} h-1 flex-1 mx-2"></div>

            <div class="relative">
                <div class="{{ ($order->status >= 4 && $order->status != 5) ? 'bg-green-600' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-check text-white"></i>
                </div>
                <div class="absolute -left-2 mt-0.5">
                    <p>Entregado</p>
                </div>
            </div>

            @elseif($order->status == 5)
            <div class="relative">
                <div class="{{ ($order->status == 5) ? 'bg-red-600' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-archive text-white"></i>
                </div>
                <div class="absolute -left-1.5 mt-0.5">
                    <p>Pedido</p>
                </div>
            </div>
            <div class="{{ ($order->status == 5) ? 'bg-red-500' : 'bg-gray-400'  }} h-1 flex-1 mx-2"></div>

            <div class="relative">
                <div class="{{ ($order->status == 5) ? 'bg-red-600' : 'bg-gray-400'  }} rounded-full h-12 w-12 flex items-center justify-center">
                    <i class="fas fa-times text-white"></i>
                </div>
                <div class="absolute -left-2 mt-0.5">
                    <p>Anulado</p>
                </div>
            </div>
            @endif

        </div>


        <div class="bg-white rounded-lg shadow-lg px-6 py-4 mb-6 flex items-center justify-between">
            <p class="text-gray-700 uppercase font-bold"><span class="font-semibold">Numero de orden:</span> Orden #
                {{ $order->id }}
            </p>

            @if ($order->status == 1)
            <div class="flex items-center justify-around">
                <div>
                    <span class="font-semibold text-red-700">Su orden no ha sido pagada</span>
                </div>
                <div>
                    <x-button-enlace class="ml-2" href="{{ route('orders.payment', $order)}}">Ir a pagar</x-button-enlace>
                </div>
            </div>
            @elseif($order->status == 2)
            <div>
                <span class="font-semibold text-yellow-700">Su orden esta siendo procesada</span>
            </div>
            @elseif($order->status == 3)
            <div>
                <span class="font-semibold text-blue-700">Su orden se encuentra en camino</span>
            </div>
            @elseif($order->status == 4)
            <div>
                <span class="font-semibold text-green-700">Su orden ya fue entregada</span>
            </div>
            @elseif($order->status == 5)
            <div>
                <span class="font-semibold text-red-700">La orden ha sido anulada</span>
            </div>
            @endif
        </div>

        <div class="bg-white rounded-lg shadow-lg p-6 mb-6">
            <div class="grid grid-cols-2 gap-6 text-gray-700">
                <div>
                    <p class="text-lg font-semibold uppercase">Envío</p>
                    @if ($order->envio_type == 1)
                    <p class="text-sm">Los productos deben ser recodigos en tienda</p>
                    <p class="text-sm">Calle falssa 123</p>
                    @else
                    <p class="text-sm">Los productos seran enviados a:</p>
                    <p class="text-sm">{{ $order->address }}</p>
                    <p>{{ $order->department->name }} - {{ $order->city->name }} -
                        {{ $order->district->name }}
                    </p>
                    @endif
                </div>
                <div>
                    <p class="text-lg font-semibold uppercase">Datos de contacto</p>

                    <p class="text-sm">Persona que recibe: {{ $order->contact }}</p>
                    <p class="text-sm">Telefono de contacto: {{ $order->phone }}</p>
                </div>
            </div>

        </div>

        <div class="bg-white rounded-lg shadow-lg p-6 text-gray-700 mb-6">
            <p class="text-lg font-semibold mb-4">Resumen</p>

            <table class="table-auto w-full">
                <thead>
                    <tr>
                        <th></th>
                        @role('Administrador|ClientePreferente')
                        <th>Puntos</th>
                        @endrole
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($items as $item)
                    <tr>
                        <td>
                            <div class="flex">
                                <img class="h-15 w-20 object-cover rounded-lg mr-4" src="{{ $item->options->image }}" alt="">
                                <article>
                                    <h1 class="font-bold">{{ $item->name }}</h1>
                                    <div class="flex text-xs">
                                        @isset($item->options->option)
                                        Opcion: {{ $item->options->option }}
                                        @endisset
                                    </div>
                                </article>
                            </div>
                        </td>
                        @role('Administrador|ClientePreferente')
                        <td class="text-center">
                            <p>{{$item->options->pointssell * $item->qty}}</p>
                        </td>
                        @endrole
                        <td class="text-center">
                            $ {{ $item->price }} MXN
                        </td>
                        <td class="text-center">
                            {{ $item->qty }}
                        </td>
                        <td class="text-center">
                            $ {{ $item->price * $item->qty }} MXN
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</x-app-layout>