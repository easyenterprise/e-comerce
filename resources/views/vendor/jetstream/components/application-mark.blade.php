@props(['height' => '30', 'width' => '36',  'attributes' => ''])

@php
switch ($width) {
    case '36':
        $width = 'w-36';
        break;
    case '20':
        $width = 'w-20';
        break;
    case '96':
        $width = 'w-96';
        break;
}

switch ($height) {
    case '30':
        $height = 'h-30';
        break;
    case '16':
        $height = 'h-16';
        break;
    case '96':
        $height = 'h-96';
        break;
}

@endphp

<img class="{{$height}} {{ $width }}" src="{{asset('img/logo/logo.png')}}"  {{ $attributes }} >
