<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === "TUGO | Club de Compras")
<x-jet-application-mark style="height: 150px" />
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
