<div class="js-cookie-consent cookie-consent fixed bottom-0 inset-x-0 bg-skyColor1-700 shadow-lg">
    <div class="max-w-7xl mx-auto px-6">
        <div class="py-6">
            <div class="md:flex md:justify-between md:items-center pb-2">
                <div class="pb-2">
                    <p class="ml-3 text-white cookie-consent__message">
                        {!! trans('cookie-consent::texts.message') !!}
                    </p>
                </div>
                <div class="">
                    <a
                        class="js-cookie-consent-agree cookie-consent__agree cursor-pointer shadow-lg flex items-center justify-center px-4 py-2 rounded-md text-sm font-medium text-skyColor1-700 bg-white hover:bg-skyColor1-500 hover:text-white">
                        {{ trans('cookie-consent::texts.agree') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
