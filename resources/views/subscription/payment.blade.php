<x-app-layout>

    aqui el resumen de la suscripciones

    {{-- @php
        // SDK de Mercado Pago
        require base_path('/vendor/autoload.php');
        // Agrega credenciales
        MercadoPago\SDK::setAccessToken(config('services.mercadopago.token'));
        
        // Crea un objeto de preferencia
        $preference = new MercadoPago\Preference();
        $shipments = new MercadoPago\Shipments();

        $shipments->cost = $order->shipping_cost;
        $shipments->mode = "not_specified";

        $preference->shipments = $shipments;
        
        // Crea un ítem en la preferencia
        foreach ($items as $producto) {
            $item = new MercadoPago\Item();
            $item->title = 'Producto';
            $item->quantity = $producto->qty;
            $item->unit_price = $producto->price;
        
            $products[] = $item;
        }
        
        $preference->back_urls = [
            //Verifiacion de rutas cuando este en produccion
            'success' => route('orders.pay', $order),
            'failure' => route('orders.pay', $order),
            'pending' => route('orders.pay', $order),
        ];
        $preference->auto_return = 'approved';
        
        $preference->items = $products;
        $preference->save();
    @endphp

    <div class="container py-8">
        <div class="bg-white rounded-lg shadow-lg px-6 py-4 mb-6">
            <p class="text-gray-700 uppercase"><span class="font-semibold">Numero de orden:</span> <span class="font-bold">Orden
                #{{ $order->id }}</span></p>
        </div>

        <div class="bg-white rounded-lg shadow-lg p-6 mb-6">
            <div class="grid grid-cols-2 gap-6 text-gray-700">
                <div>
                    <p class="text-lg font-semibold uppercase">Envío</p>
                    @if ($order->envio_type == 1)
                        <p class="text-sm">Los productos deben ser recodigos en tienda</p>
                        <p class="text-sm">Calle falssa 123</p>
                    @else
                        <p class="text-sm">Los productos seran enviados a:</p>
                        <p class="text-sm">{{ $order->address }}</p>
                        <p>{{ $order->department->name }} - {{ $order->city->name }} -
                            {{ $order->district->name }}</p>
                    @endif
                </div>
                <div>
                    <p class="text-lg font-semibold uppercase">Datos de contacto</p>

                    <p class="text-sm">Persona que recibe: {{ $order->contact }}</p>
                    <p class="text-sm">Telefono de contacto: {{ $order->phone }}</p>
                </div>
            </div>

        </div>

        <div class="bg-white rounded-lg shadow-lg p-6 text-gray-700 mb-6">
            <p class="text-lg font-semibold mb-4">Resumen</p>

            <table class="table-auto w-full">
                <thead>
                    <tr>
                        <th></th>
                        @role('Administrador|ClientePreferente')
                        <th>Puntos</th>
                        @endrole
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody class="">
                    @foreach ($items as $item)
                        <tr>
                            <td>
                                <div class="flex">
                                    <img class="h-15 w-20 object-cover mr-4 rounded-lg" src="{{ $item->options->image }}" alt="">
                                    <article>
                                        <h1 class="font-bold">{{ $item->name }}</h1>
                                        <div class="flex text-xs">
                                            @isset($item->options->option)
                                                Opcion: {{ $item->options->option }}
                                            @endisset
                                        </div>
                                    </article>
                                </div>
                            </td>
                            @role('Administrador|ClientePreferente')
                            <td class="text-center">
                                {{ $item->options->pointssell * $item->qty }}
                            </td>
                            @endrole
                            <td class="text-center">
                                $ {{ $item->price }} MXN
                            </td>
                            <td class="text-center">
                                {{ $item->qty }}
                            </td>
                            <td class="text-center">
                                $ {{ $item->price * $item->qty }} MXN
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="bg-white rounded-lg shadow-lg p-6 flex justify-between items-center">
            <img class="h-10" src="{{ asset('img/visa-mastercard.png') }}" alt="visa">
            <div class="text-gray-700">
                <p class="text-sm font-semibold">
                    Subtotal: $ {{ $order->total - $order->shipping_cost }} MXN
                </p>
                <p class="text-sm font-semibold">
                    Envio: $ {{ $order->shipping_cost }} MXN
                </p>
                <p class="text-lg font-bold uppercase">
                    Total: $ {{ $order->total }} MXN
                </p>
                <div class="cho-container">

                </div>
            </div>
        </div>

    </div>

    <script src="https://sdk.mercadopago.com/js/v2"></script>
    <script>
        // Agrega credenciales de SDK
        const mp = new MercadoPago("{{ config('services.mercadopago.key') }}", {
            locale: 'es-AR'
        });

        // Inicializa el checkout
        mp.checkout({
            preference: {
                id: '{{ $preference->id }}'
            },
            render: {
                container: '.cho-container', // Indica el nombre de la clase donde se mostrará el botón de pago
                label: 'Pagar', // Cambia el texto del botón de pago (opcional)
            }
        });
    </script> --}}

</x-app-layout>