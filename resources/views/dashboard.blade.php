<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="container">
            <div class="grid grid-cols-1 gap-6 sm:grid-cols-2">
                <div class="col-span-2 md:col-span-1">
                    <div class=" bg-white py-4 rounded-lg shadow-lg">
                        <div class="mt-4 border-b-2 py-2 border-gray-200 flex flex-col items-center">
                            <img class="rounded-md shadow-2xl object-cover w-24 h-24 sm:w-44 sm:h-44" src="{{ auth()->user()->profile_photo_url}}" alt="{{ auth()->user()->name }}">
                            <h2 class="text-2xl font-bold mt-2">{{ auth()->user()->name }}</h2>
                            @if (@Auth::user()->hasRole('Administrador'))
                            <span class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 bg-red-700 rounded">Administrador</span>
                            @endif
                            @if (@Auth::user()->hasRole('ClientePreferente'))
                                <span class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-green-100 bg-green-700 rounded">Cliente Preferente</span>
                            @endif
                            @if (@Auth::user()->hasRole('ClienteNormal'))
                            <span class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-gray-100 bg-gray-700 rounded">Cliente Normal</span>
                            @endif
                        </div>
                        <div>
                            <div class="grid lg:grid-cols-3 sm:grid-cols-2 py-2 px-4">
                                <div class="mt-4">
                                    <h2 class="ml-4 text-indigo-700"><i class="fas fa-phone"></i> Telefono</h2>
                                    <p class="ml-6">{{ auth()->user()->phone }}</p>
                                </div>
                                <div class="mt-4">
                                    <h2 class="ml-4 text-indigo-700"><i class="fas fa-envelope"></i> Correo</h2>
                                    <p class="ml-6"> {{ Str::limit( auth()->user()->email ,22)}} </p>
                                </div>
                                <div class="mt-4">
                                    <h2 class="ml-4 text-indigo-700"><i class="fas fa-address-card"></i> CURP</h2>
                                    <p class="ml-6">{{ auth()->user()->curp }}</p>
                                </div>
                                @if(@Auth::user()->hasRole('ClientePreferente') || @Auth::user()->hasRole('Administrador') )
                                    <div class="mt-4">
                                        <h2 class="ml-4 text-indigo-700"><i class="fas fa-gift"></i> Puntos</h2>
                                        <p class="ml-6">{{ auth()->user()->points}} pts.</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-span-2 md:col-span-1 border-2 border-red-600 p-4"></div>
            </div>
        </div>
    </div>
</x-app-layout>
