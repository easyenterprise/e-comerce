@if ($errors->any())
    <div class="bg-red-100 border-t border-r border-l border-b border-red-500 text-red-700 px-4 py-3 rounded mb-2"
        role="alert">
        <p class="font-bold">Error</p>
        @foreach ($errors->all() as $message)
            <p class="text-sm"><i class="fas fa-exclamation-circle"></i> {{ $message }}</p>
        @endforeach
    </div>
@endif
