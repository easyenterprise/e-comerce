<div class="bg-gray-800">
    <div class="grid md:grid-cols-4 gap-6 container p-8 border-b-2 border-indigo-500">
        <div class="flex w-full justify-center items-center md:block">
            <img class="h-44 w-52" src="{{ asset('img/logo/logo-blanco.png')}}" alt="">
        </div>
        <div class="flex flex-col items-center md:block">
            <h1 class="text-xl font-bold text-indigo-400">Recursos</h1>
            <ul class="text-center md:text-left"> 
                <li><a href="#" class="text-white md:ml-3">Item 1</a></li>
                <li><a href="#" class="text-white md:ml-3">Item 2</a></li>
                <li><a href="#" class="text-white md:ml-3">Item 3</a></li>
            </ul>
        </div>
        <div class="flex flex-col items-center md:block">
            <h1 class="text-xl font-bold text-indigo-400">Legal</h1>
            <ul class="text-center md:text-left">
                <li><a href="#" class="text-white md:ml-3">Privacidad</a></li>
                <li><a href="#" class="text-white md:ml-3">Terminos de Servicio</a></li>
            </ul>
        </div>
        <div class="flex items-center justify-around text-indigo-500 mt-4 md:mt-0">
            <a href="#"><i class="fab fa-facebook-square text-5xl hover:text-indigo-300"></i></a>
            <a href="#"><i class="fab fa-twitter-square text-5xl hover:text-indigo-300"></i></a>
            <a href="#"><i class="fab fa-instagram text-5xl hover:text-indigo-300"></i></a>
        </div>
    </div>
    <div class="container flex justify-center py-4 text-white font-bold">
        <span class="text-center">&copy; 2022 TUGO - Club de Compras. Todos los derechos reservados</span>
    </div>
</div>