@if (session('status'))
    <div x-data="{ isVisible: true }" x-init="
        setTimeout(() => {
            isVisible = false;
        }, 8000)" x-show.transition.duration.1000ms="isVisible"
        class="bg-green-100 border-t border-r border-l border-b border-green-500 text-gren-700 px-4 py-3 rounded mb-2"
        role="alert">
        <p class="font-bold">Exito</p>
        <p class="text-sm"><i class="fas fa-check-circle"></i> {{ session('status') }}</p>
    </div>
@endif
