@props(['active'])

@php
$classes = ($active ?? false)
            ? 'text-indigo-700 font-bold h-full bg-white flex items-center p-4'
            : 'hover:bg-white hover:text-indigo-700 p-4 text-white font-bold cursor-pointer h-full flex items-center';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
