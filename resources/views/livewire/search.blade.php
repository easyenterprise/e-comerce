<div class="flex-1 relative" x-data>
    <form action="{{route('search')}}" autocomplete="off">
        <x-jet-input wire:model="search" name="name" type="text" class="w-full " placeholder="¿Estas buscando algun producto?" />
        <button class="absolute top-0 right-0 w-12 h-full bg-indigo-700 flex items-center justify-center rounded-r-md ">
            <x-search size="35" />
        </button>
    </form>

    <div class="absolute w-full hidden" :class="{ 'hidden': !$wire.open }" @click.away="$wire.open = false">
        <div class="bg-indigo-50 shadow mt-3">
            <div class="px-4 py-3 space-y-1">
                @forelse ($products as $product)
                    <a class="flex" href="{{ route('products.show', $product) }}">
                        <img class="w-16 h-12 object-cover rounded-sm"
                            src="{{ Storage::url($product->images->first()->url) }}" alt="">
                        <div class="ml-4 text-gray-800">
                            <p class="text-lg font-bold leading-5">{{ $product->name }}</p>
                            <p class="">Categoria: <span class="text-indigo-900 font-semibold">{{ $product->subcategory->category->name }}</span></p>

                        </div>
                    </a>
                @empty
                    <p class="text-lg leading-5 font-bold">
                        No existe ningun registro con los parametros especificados
                    </p>
                @endforelse
            </div>

        </div>

    </div>
</div>
