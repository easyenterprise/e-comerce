<div x-data>
    <p class="text-2xl font-semibold text-gray-700 my-2">
        @if ($price_option)
            $ {{ $price_option }} MXN
        @else
            <div class="bg-white p-2 shadow rounded-lg flex items-center justify-center">
                <p class="text-skyColor1-600 font-semibold">Seleccione una opcion para ver el precio</p>
            </div>
        @endif
    </p>

    @role('Administrador|ClientePreferente')
    <div class="flex flex-col items-center justify-center rounded-md bg-greenLime-600 my-2 text-white">
        <p class="font-semibold">Acumulas {{ $product->points_sell }} puntos al comprar</p>
        <p>ó</p>
        <p class="font-semibold">Para comprar necesita {{ $product->points_buy }} puntos</p>
    </div>
    @endrole
    <div class="bg-white rounded-lg shadow-lg my-4">
        <div class="p-4 flex items-center">
            <span class="flex items-center justify-center h-10 w-10 rounded-full bg-greenLime-600">
                <i class="fas fa-truck text-sm text-white"></i>
            </span>
            <div class="ml-4">
                <p class="text-lg font-semibold text-greenLime-600">Se hace envios a algunas areas locales
                </p>
                <p>Recibelo el {{ Date::now()->addDay(7)->format('l j F') }}</p>
            </div>
        </div>
    </div>

    <p class="text-xl text-gray-700">Opción</p>
    <select wire:model="option_id" class="form-control w-full">
        <option value="" selected disabled>Seleccionar una opcion</option>
        @foreach ($options as $option)
            <option class="capitalize" value="{{ $option->id }}">{{ $option->name }}</option>
        @endforeach
    </select>
    <p class="text-gray-700 my-4">
        <div class="flex items-baseline mb-4">
            <span class="font-semibold text-lg">Cantidad disponible: </span>
            @if ($quantity)
                <p class="ml-1">{{ $quantity }}</p>
            @elseif($quantity == 0)
            <div class="bg-white p-2 shadow rounded-lg flex items-center justify-center ml-2">
                <p class="text-skyColor1-600 font-semibold">Seleccione una opcion para ver el la cantidad</p>
            </div>
            @else
                <p class="ml-1">{{ $quantity }}</p>
            @endif
        </div>
    </p>
    <div class="flex">
        <div class="mr-4">
            <x-jet-secondary-button disabled x-bind:disabled="$wire.qty <= 1" wire:loading.attr="disabled"
                wire:target="decrement" wire:click="decrement">
                -
            </x-jet-secondary-button>

            <span class="mx-2 text-gray-700">{{ $qty }}</span>

            <x-jet-secondary-button x-bind:disabled="$wire.qty >= $wire.quantity" wire:loading.attr="disabled"
                wire:target="increment" wire:click="increment">
                +
            </x-jet-secondary-button>
        </div>
        <div class="flex-1">
            <x-button x-bind:disabled="!$wire.quantity" color="blue" class="w-full" wire:click="addItem"
                wire:loading.attr="disabled" wire:target="addItem">
                Agregar a carrito de compras
            </x-button>
        </div>
    </div>
</div>
