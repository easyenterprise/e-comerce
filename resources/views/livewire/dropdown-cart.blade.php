<div>
    <x-jet-dropdown width="96">

        <x-slot name="trigger">
            {{-- <span class="relative inline-block cursor-pointer">
                {{-- <x-cart class="pr-6" color="blue" size="40" />
                <i class="mx-8 fas fa-shopping-bag text-3xl text-indigo-700 left-0"></i>
                <span class="absolute top-0 right-0 inline-flex items-center mt-1 justify-center px-2 py-1 text-xs font-bold leading-none text-indigo-700 transform translate-x-1/2 -translate-y-1/2">Mi Carrito</span>

                @if (Cart::count())
                    <span class="absolute top-4 right-0 inline-flex items-center mt-1 justify-center px-4 py-1 text-xs font-bold leading-none text-white transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full">{{Cart::count() }}</span>
                @endif    
            </span> --}}
            <div class="flex col-span-1 cursor-pointer">
                <div class="flex items-center justify-end">
                    <i class="fas fa-shopping-bag text-3xl text-indigo-700"></i>
                </div>
                <div class="flex flex-col justify-center items-center ml-2">
                    <h5 class="text-indigo-700 font-bold">Mi Carrito</h5>
                    @if (Cart::count())
                        <span class="px-4 py-1 text-xs font-bold bg-red-700 text-white rounded-full">{{Cart::count() }}</span>
                    @endif 
                </div>
            </div>
        </x-slot>

        <x-slot name="content">
            <ul>
                @forelse (Cart::content() as $item)
                    <li class="flex p-2 border-b border-gray-200">
                        <img class="h-15 w-20 object-cover mr-4 rounded" src="{{ $item->options->image}}" alt="">
                        <article class="flex-1">
                            <h1 class="font-bold">{{ $item->name}}</h1>
                            <div class="flex">
                                <p>Cant: {{$item->qty}}</p>
                                @isset($item->options['option'])
                                    <p class="mx-2"> - Opción {{$item->options['option']}}</p>
                                @endisset
                            </div>
                            <p>MXN {{$item->price}}</p>
                            {{-- <p>Puntos de venta {{$item->options['pointssell'] * $item->qty}}</p>
                            <p>Puntos de compra {{$item->options['pointsbuy'] * $item->qty }}</p> --}}
                        </article>
                    </li>
                @empty
                <li class="py-6 px-4">
                    <p class="text-center text-red-700 font-bold">
                        No tiene agregado ningun item en el carrito
                    </p>
                </li>
                @endforelse
            </ul>
            @if (Cart::count())
                <div class="py-2 px-3">
                    <p class="text-lg text-gray-700 mt-2 mb-3"><span class="font-bold">Total:</span> {{Cart::subtotal()}} MXN</p>
                    <x-button-enlace class="w-full cursor-pointer" color="indigo" href="{{route('shopping-cart')}}">Ir al carrito de compras</x-button-enlace>
                </div>
            @endif
        </x-slot>

    </x-jet-dropdown>
</div>
<div class="ml-6"></div>
