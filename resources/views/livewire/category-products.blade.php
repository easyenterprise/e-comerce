<div wire:init="loadPosts">
    @if (count($products))
        <div class="glider-contain">
            <ul class="glider-{{ $category->id }}">
                @foreach ($products as $product)
                    <li class="rounded bg-white shadow-md {{ $loop->last ? '' : 'sm:mr-4' }}">
                        <a href="{{ route('products.show' , $product)}}">
                            <article>
                                <figure>
                                    <img class="h-48 w-full object-cover object-center rounded-t-md"
                                        src="{{ Storage::url($product->images->first()->url) }}">
                                </figure>
                                <div class="py-4 px-3">
                                    <h1 class="text-lg font-medium flex justify-between text-indigo-700">
                                            {{ Str::limit($product->name, 15) }}
                                            <span class="font-semibold">{{ round($product->reviews->avg('rating'), 1)}}<li class="fas fa-star ml-1 text-sm text-yellow-400"></span>
                                    </h1>
                                    @if ($product->price)
                                        <p class="font-semibold text-gray-900">${{ $product->price }} MXN</p>
                                    @else
                                        <p class="font-semibold text-red-500">Opciones Disponibles</p>
                                    @endif
                                </div>
                            </article>
                        </a>
                    </li>
                @endforeach
            </ul>

            <button aria-label="Previous" class="glider-prev">«</button>
            <button aria-label="Next" class="glider-next">»</button>
            <div role="tablist" class="dots"></div>
        </div>
    @else
        <div class="mb-4 h-48 flex justify-center items-center bg-white shadow-xl border border-gray-100 rounded-lg">
            {{-- <div class="rounded-xl animate-spin ease duration-300 w-10 h-10 border-2 border-indigo-700"></div> --}}
            <svg class="animate-spin -ml-1 mr-3 h-14 w-14 text-indigo-700" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
            </svg>
            <p class="text-indigo-800 font-semibold text-xl">Cargando...</p>
        </div>

    @endif

</div>
