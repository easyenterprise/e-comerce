<div class="container py-8 grid grid-cols-5 gap-6">
    
    {{-- TARGETA DE DATOS IZQUIERDA --}}
    <div class="col-span-3">

        {{-- Datos del usuario para el envio --}}
        <div class="bg-white rounded-lg shadow p-6">
            <x-auth-validation-errors class="mb-4" />
            <div class="mb-4">
                <x-jet-label value="Nombre de contacto" />
                <x-jet-input type="text" wire:model.defer="contact" placeholder="Ingrese nombre completo" pattern="[A-Za-z0-9]"
                    class="w-full" />
            </div>

            <div>
                <div>
                    <x-jet-label value="Telefono de contacto" />
                    <x-jet-input type="tel" maxlength="10" wire:model.defer="phone" placeholder="Ingrese un numero de contacto" 
                        class="w-full" />
                </div>
            </div>
        </div>

        
        {{-- Datos del envio --}}
        <div x-data="{ envio_type : @entangle('envio_type') }">
            <P class="mt-6 mb-3 text-lg text-gray-700 font-semibold">Envios</P>
            <label class="bg-white rounded-lg shadow px-6 py-4 flex items-center mb-4 cursor-pointer">
                <input x-model="envio_type" type="radio" value="1" name="envio_type" class="text-gray-600">
                <span class="ml-2 text-gray-700">
                    Recogo en tienda (calle falsa 123)
                </span>
                <span class="font-semibold text-gray-700 ml-auto">
                    Gratis
                </span>
            </label>

            <div class="bg-white rounded-lg shadow">
                <label class="px-6 py-4 flex items-center cursor-pointer">
                    <input x-model="envio_type" type="radio"  value="2" name="envio_type" class="text-gray-600">
                    <span class="ml-2 text-gray-700">
                        Envio a domicilio
                    </span>
                </label>
                <div class="px-6 pb-6 grid grid-cols-2 gap-6 hidden" :class="{ 'hidden' : envio_type != 2 }">
                    {{-- Departamento --}}
                    <div>
                        <x-jet-label value="Departamento" />
                        <select class="form-control w-full" wire:model="department_id">
                            <option value="" disabled selected>Seleccione un Departamento</option>
                            @foreach ($departments as $department)
                                <option value="{{ $department->id }}">{{ $department->name }}</option>

                            @endforeach
                        </select>
                    </div>

                    {{-- Ciudades --}}
                    <div>
                        <x-jet-label value="Ciudad" />
                        <select class="form-control w-full" wire:model="city_id">
                            <option value="" disabled selected>Seleccione una Ciudad</option>
                            @foreach ($cities as $city)
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    {{-- Distritos --}}
                    <div>
                        <x-jet-label value="Distrito" />
                        <select class="form-control w-full" wire:model="district_id">
                            <option value="" disabled selected>Seleccione un distrito</option>
                            @foreach ($districts as $district)
                                <option value="{{ $district->id }}">{{ $district->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div>
                        <x-jet-label value="Direccion" />
                        <x-jet-input class="w-full" wire:model="address" type="text" />
                    </div>

                    <div class="col-span-2">
                        <x-jet-label value="Referencia" />
                        <x-jet-input class="w-full" wire:model="references" type="text"/>
                    </div>

                </div>
            </div>
        </div>

        <div>
            <x-jet-button class="mt-6 mb-4" 
            wire:loading.attr="disabled"
            wire:target="create_order"
            wire:click="create_order">
                Continuar con la compra
            </x-jet-button>
            <div class="border-b border-gray-400 my-2"></div>
            <p class="text-sm text-gray-700 mt-2 text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Impedit cumque repudiandae magni ratione pariatur nesciunt, corrupti quae illo qui delectus eaque
                temporibus? Enim omnis nisi temporibus! Ex neque eveniet tenetur. <a href=""
                    class="font-semibold text-skyColor1-500">{{ __('Privacy Policy') }}</a> </p>
        </div>

    </div>

    {{-- TARGETA DE DATOS DERECHA --}}
    <div class="col-span-2">
        <div class="bg-white rounded-lg shadow p-6">
            <ul>
                @forelse (Cart::content() as $item)
                    <li class="flex p-2">
                        <img class="h-15 w-20 object-cover mr-4 rounded" src="{{ $item->options->image }}" alt="">
                        <article class="flex-1">
                            <h1 class="font-bold">{{ $item->name }}</h1>
                            <div class="flex">
                                <p>Cant: {{ $item->qty }}</p>
                                @isset($item->options['option'])
                                    <p class="mx-2"> - Opción {{ $item->options['option'] }}</p>
                                @endisset
                            </div>
                            <p>MXN {{ $item->price }}</p>
                        </article>
                    </li>
                @empty
                    <li class="py-6 px-4">
                        <p class="text-center text-gray-700 ">
                            No tiene agregado ningun item en el carrito
                        </p>
                    </li>
                @endforelse
            </ul>
            <hr class="mt-4 mb-3">

            <div class="text-gray-700">
                <p class=" flex justify-between items-center">
                    Subtotal
                    <span class="font-semibold">$ {{ Cart::subtotal() }} MXN</span>
                </p>
                <p class=" flex justify-between items-center">
                    Envio
                    <span class="font-semibold">
                    @if ($envio_type == 1 || $shipping_cost == 0)
                        Gratis
                    @else
                        $ {{$shipping_cost}} MXN
                    @endif
                    </span>
                </p>

                <hr class="mt-4 mb-3">

                <p class=" flex justify-between items-center font-semibold">
                    <span class="text-lg">Total</span>
                    @if ($envio_type == 1)
                        $ {{ Cart::subtotal() }} MXN
                    @else
                        $ {{ Cart::subtotal() + $shipping_cost }} MXN
                    @endif
                </p>

            </div>
        </div>

    </div>

</div>
