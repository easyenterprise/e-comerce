<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-600 leading-tight">
            Tablero
        </h2>
    </x-slot>
</div>


<!-- component -->
<div class="container flex justify-center items-center mt-4">
    <div class="container grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 justify-center">
        {{-- ORDENES --}}
        <div class="relative rounded-lg bg-red-400 shadow-lg h-36 border-2 border-red-900 border-opacity-50">
            <i class="fas fa-clipboard-check absolute right-4 top-1 text-7xl opacity-30"></i>
            <h1 class="absolute top-5 left-2 text-4xl text-white">Ordenes</h1>
            <span class="absolute bottom-0 left-5 text-6xl text-white font-semibold">{{ $orders->count() }}</span>
        </div>

        {{-- PRODUCTOS --}}
        <div class="relative rounded-lg bg-yellow-400 shadow-lg h-36 border-2 border-yellow-900 border-opacity-50">
            <i class="fab fa-product-hunt absolute right-2 top-1 text-7xl opacity-30"></i>
            <h1 class="absolute top-5 left-2 text-4xl text-white">Productos</h1>
            <span class="absolute bottom-0 left-5 text-6xl text-white font-semibold">{{ $products->count() }}</span>
        </div>

        {{-- CATEGORIAS --}}
        <div class="relative rounded-lg bg-indigo-400 shadow-lg h-36 border-2 border-indigo-900 border-opacity-50">
            <i class="fab fa-cuttlefish absolute right-2 top-1 text-7xl opacity-30"></i>
            <h1 class="absolute top-5 left-2 text-4xl text-white">Categorias</h1>
            <span class="absolute bottom-0 left-5 text-6xl text-white font-semibold">{{ $categories->count() }}</span>
        </div>

        {{-- MARCAS --}}
        <div class="relative rounded-lg bg-blue-400 shadow-lg h-36 border-2 border-green-900 border-opacity-50">
            <i class="fas fa-users absolute right-2 top-1 text-7xl opacity-30"></i>
            <h1 class="absolute top-5 left-2 text-4xl text-white">Marcas</h1>
            <span class="absolute bottom-0 left-5 text-6xl text-white font-semibold">{{ $brands->count() }}</span>
        </div>

        {{-- USUARIOS --}}
        <div class="relative rounded-lg bg-green-400 shadow-lg h-36 border-2 border-green-900 border-opacity-50">
            <i class="fas fa-users absolute right-2 top-1 text-7xl opacity-30"></i>
            <h1 class="absolute top-5 left-2 text-4xl text-white">Usuarios</h1>
            <span class="absolute bottom-0 left-5 text-6xl text-white font-semibold">{{ $users->count() }}</span>
        </div>

        {{-- SUBCATEGORIAS --}}
        <div class="relative rounded-lg bg-purple-400 shadow-lg h-36 border-2 border-purple-900 border-opacity-50">
            <i class="fab fa-cuttlefish absolute right-2 top-1 text-7xl opacity-30"></i>
            <h1 class="absolute top-5 left-2 text-3xl text-white">Subcategorias</h1>
            <span class="absolute bottom-0 left-5 text-6xl text-white font-semibold">{{ $subcategories->count() }}</span>
        </div>
        
    </div>
</div>