<div>
    <x-slot name="header">
        <div class="flex items-center">
            <h2 class="font-semibold text-xl text-gray-600 leading-tight">
                Usuarios
            </h2>
            <x-button-enlace class="ml-auto" color="green" href="{{ route('admin.products.create') }}">
                Agregar Usuario
            </x-button-enlace>
        </div>
    </x-slot>

    {{-- Tabla de Productos --}}
    <div class="container py-12">
        <x-table-responsive>
            <div class="px-6 py-4 bg-gray-300">
                <x-jet-input class="w-full" type="text" wire:model="search"
                    placeholder="Ingrese el nombre ó correo del usuario" />
            </div>

            @if ($users->count())
                <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-gray-50">
                        <tr>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Foto
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                id
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                nombre
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                correo
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Puntos
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                rol
                            </th>
                            <th scope="col" class="relative px-6 py-3">
                                <span class="sr-only">Editar</span>
                            </th>
                            <th scope="col" class="relative px-6 py-3">
                                <span class="sr-only">Eliminar</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                        @foreach ($users as $user)
                            @if (!empty($user->getRoleNames()))
                                @foreach ($user->getRoleNames() as $rol)
                                    @if ($rol == 'Administrador')
                                        
                                    @else
                                    <tr>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="flex items-center">
                                                    <div class="flex-shrink-0 h-10 w-10">
                                                        <img class="h-10 w-10 rounded-full object-cover" src="{{ $user->getProfilePhotoUrlAttribute() }}" alt="">
                                                    </div>
                                            </div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">{{ $user->id }}
                                            </div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">{{ $user->name }}
                                            </div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">{{ $user->email }}
                                            </div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">{{ $user->points }}
                                            </div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            @if (!empty($user->getRoleNames()))
                                                @foreach ($user->getRoleNames() as $rol)
                                                    @if ($rol == 'ClientePreferente')
                                                        <span class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-green-800 bg-green-300 rounded-full">{{$rol}}</span>
                                                    @else
                                                        <span class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-white bg-gray-600 rounded-full">{{$rol}}</span>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </td>
                                        <td class="whitespace-nowrap text-right text-sm font-medium">
                                            <div class="px-2 py-1 bg-yellow-400 rounded-lg text-center">
                                                <a href="#"
                                                    class="text-yellow-800 hover:text-white">Editar</a>
                                            </div>
                                        </td>
                                        <td class="px-2 whitespace-nowrap text-right text-sm font-medium">
                                            <div class="px-2 py-1 bg-red-400 rounded-lg text-center">
                                                <a href="#"
                                                    class="text-red-800 hover:text-white">Elimnar</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                    
                                @endforeach
                            @endif

                        @endforeach

                        <!-- More people... -->
                    </tbody>
                </table>
            @else
                <div class="px-6 py-4 text-center bg-white">
                    No existe ningun registro coincidente
                </div>
            @endif
            @if ($users->hasPages())
                <div class="px-6 py-4 bg-gray-200">
                    {{ $users->links() }}
                </div>
            @endif

        </x-table-responsive>
    </div>

</div>
