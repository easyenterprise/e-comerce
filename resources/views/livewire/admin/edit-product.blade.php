<div>
    <header class="bg-white shadow">
        <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <div class="flex justify-between items-center">
                <h1 class="font-semibold text-xl text-gray-800 leading-tight">Productos</h1>
                <x-button-enlace wire:click="$emit('deleteProduct')" class="cursor-pointer">
                    Eliminar
                </x-button-enlace>
            </div>
        </div>
    </header>
    
    <div class="max-w-4xl mx-auto px-4 sm:px-6 lg:px-8 py-12 text-gray-800">
        <h1 class="text-3xl text-center font-semibold m-8">Editar Producto #{{ $product->id }}</h1>
        <div class="mb-4" wire:ignore>
            <form action="{{ route('admin.products.files', $product) }}" method="POST" class="dropzone"
                id="my-great-dropzone"></form>
        </div>
        @if ($product->images->count())
            <section class="bg-white shadow-xl rounded-lg p-6 mb-4">
                <h1 class="text-2xl text-center font-semibold mb-2">Imagenes de Producto</h1>
                <ul class="flex flex-wrap">
                    @foreach ($product->images as $image)
                        <li class="relative" wire:key="image-{{ $image->id }}">
                            <img class="w-32 h-20 object-cover mx-4 my-2 rounded-lg shadow-lg"
                                src="{{ Storage::url($image->url) }}" alt="">
                            <x-jet-danger-button class="absolute right-4 top-2"
                                wire:click="deleteImage({{ $image->id }})" wire:loading.attr="disabled"
                                wire:target="deleteImage({{ $image->id }})">
                                X
                            </x-jet-danger-button>
                        </li>
                    @endforeach
                </ul>
            </section>
        @endif

        {{-- CAMBIAR STATUS DEL PRODUCTO --}}
        @livewire('admin.status-product', ['product' => $product], key('status-product-' . $product->id))

        <x-auth-validation-errors class="mb-4" />
        <div class="bg-white shadow-xl rounded-lg p-6">
            <div class="grid grid-cols-2 gap-6 mb-4">
                {{-- Categoria --}}
                <div>
                    <x-jet-label value="Categorias" />
                    <select class="w-full form-control" wire:model="category_id">
                        <option value="" selected disabled>Seleccione una categoría</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>

                {{-- Subcategoria --}}
                <div>
                    <x-jet-label value="Subcategorias" />
                    <select class="w-full form-control" wire:model="product.subcategory_id">
                        <option value="" selected disabled>Seleccione una subcategoría</option>
                        @foreach ($subcategories as $subcategory)
                            <option value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            {{-- Nombre del producto --}}
            <div class="mb-4">
                <x-jet-label value="Nombre" />
                <x-jet-input type="text" wire:model="product.name" class="w-full"
                    placeholder="Ingrese nombre del producto" />
            </div>

            {{-- Slug del producto --}}
            <div class="mb-4">
                <x-jet-label value="Slug" />
                <x-jet-input disabled type="text" wire:model="slug" class="w-full bg-gray-100"
                    placeholder="Campo automatico" />
            </div>

            {{-- Descripcion --}}
            <div class="mb-4">
                <div wire:ignore>
                    <x-jet-label value="Descripcion" />
                    <textarea class="w-full form-control" rows="10" wire:model="product.description" x-data x-init="ClassicEditor
                    .create( $refs.miEditor )
                    .then(function(editor){
                        editor.model.document.on('change:data', () => {
                            @this.set('product.description', editor.getData())
                        })
                    })
                    .catch( error => {
                        console.error( error );
                    } );" x-ref="miEditor"></textarea>
                </div>
            </div>


            <div class="mb-4 grid grid-cols-2 gap-6">
                {{-- Marcas --}}
                <div>
                    <x-jet-label value="Marca" />
                    <select class="form-control w-full" wire:model="product.brand_id">
                        <option value="" selected disabled>Seleccione una marca</option>
                        @foreach ($brands as $brand)
                            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                        @endforeach
                    </select>
                </div>
                @if ($this->subcategory)
                    @if (!$this->subcategory->option)
                        <div>
                            <x-jet-label value="Precio" />
                            <x-jet-input wire:model="product.price" type="number" class="w-full" step=".001" />
                        </div>
                    @endif
                @endif
            </div>

            <div class="mb-4 grid grid-cols-2 gap-6">
                {{-- Puntos --}}
                <div>
                    <x-jet-label value="Puntos para suma de cliente" />
                    <x-jet-input wire:model="pointssell" type="number" class="w-full" />
                </div>
                <div>
                    <x-jet-label value="Puntos para compra" />
                    <x-jet-input wire:model="pointsbuy" type="number" class="w-full" />
                </div>
            </div>



            @if ($this->subcategory)
                @if (!$this->subcategory->option)
                    <div class="mb-4">
                        <x-jet-label value="Cantidad" />
                        <x-jet-input wire:model="product.quantity" type="number" class="w-full" />
                    </div>
                @endif
            @endif

            <div class="">
                <x-jet-label value=" ¿En promocion?" />
            <x-jet-input wire:model="promo" type="checkbox" /><label>Si/No</label>
        </div>


        <div class="flex mt-4 justify-end items-center">
            <x-jet-action-message class="mr-3" on="saved">
                Actualizando...
            </x-jet-action-message>
            <x-button-enlace wire:click="save" wire:loading.attr="disabled" wire:target="save" class="cursor-pointer"
                color="yellow">
                <i class="fas fa-save mr-1"></i>Guardar
            </x-button-enlace>
        </div>
    </div>

    @if ($this->subcategory)
        @if ($this->subcategory->option)
            @livewire('admin.option-product', ['product' => $product], key($product->id))
        @endif
    @endif





</div>

@push('script')
    <script>
        Dropzone.options.myGreatDropzone = { // camelized version of the `id`
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            dictDefaultMessage: "Arrastre una imagen para el producto o de click al recuadro para elegirla",
            acceptedFiles: 'image/*',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            complete: function(file) {
                this.removeFile(file);
            },
            queuecomplete: function() {
                Livewire.emit('refreshProduct');
            }
        };

        Livewire.on('deleteProduct', () => {
            Swal.fire({
                title: 'Estas seguro?',
                text: "Recuerda que esta accion no se puede revertir!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {

                    Livewire.emitTo('admin.edit-product', 'delete');

                    Swal.fire(
                        'Eliminado!',
                        'Se ha eliminado de la base de datos',
                        'success'
                    )
                }
            })
        })

        Livewire.on('deletePivot', pivot => {
            Swal.fire({
                title: 'Estas seguro?',
                text: "Recuerda que esta accion no se puede revertir!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {

                    Livewire.emitTo('admin.option-product', 'delete', pivot);

                    Swal.fire(
                        'Eliminado!',
                        'Se ha eliminado de la base de datos',
                        'success'
                    )
                }
            })
        })
    </script>
@endpush
</div>
