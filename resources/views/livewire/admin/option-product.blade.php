<div>
    <div class="my-4 bg-white shadow-lg rounded-lg p-6">
        <x-auth-validation-errors class="mb-4" />
        {{-- Opcion --}}
        <div class="mb-6">
            <x-jet-label>
                Opción
            </x-jet-label>
            <div class="grid grid-cols-6 gap-6">
                @foreach ($options as $option)
                    <label>
                        <input type="radio" wire:model.defer="option_id" name="option_id" value="{{ $option->id }}">
                        <span class="ml-2 text-gray-700 capitalize">{{ $option->name }}</span>
                    </label>
                @endforeach
            </div>
        </div>

        {{-- Cantidad --}}
        <div class="mb-4 grid grid-cols-2 gap-6">
            <div>
                <x-jet-label>
                    Cantidad
                </x-jet-label>
                <x-jet-input type="number" wire:model.defer="quantity" placeholder="Ingrese una cantidad"
                    class="w-full" />
            </div>
            <div>
                <x-jet-label>
                    Precio
                </x-jet-label>
                <x-jet-input type="number" wire:model.defer="price" placeholder="Ingrese un precio"
                    class="w-full" />
            </div>
        </div>
        <div class="flex mt-4 justify-end items-center">
            <x-jet-action-message class="mr-3" on="saved">
                Agregado
            </x-jet-action-message>
            <x-button-enlace wire:click="save" wire:loading.attr="disabled" wire:target="save" class="cursor-pointer"
                color="yellow">
                <i class="fas fa-save mr-1"></i>Agregar
            </x-button-enlace>
        </div>
    </div>

    @if ($product_options->count())
    <div class="bg-white shadow-lg rounded-lg p-6">
        <table>
            <thead>
                <tr>
                    <th class="px-4 py-2 w-1/3">Opcion</th>
                    <th class="px-4 py-2 w-1/3">Cantidad</th>
                    <th class="px-4 py-2 w-1/3">Precio</th>
                    <th class="px-4 py-2 w-1/3"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($product_options as $product_option)
                    <tr wire:key="product_option-{{ $product_option->pivot->id }}">
                        <td class="capitalize px-4 py-2 text-center">
                            {{ $options->find($product_option->pivot->option_id)->name }}</td>
                        <td class="px-4 py-2 text-center">
                            {{ $product_option->pivot->quantity }}
                        </td>
                        <td class="px-4 py-2 text-center">
                            $ {{ $product_option->pivot->price }}
                        </td>
                        <td class="px-4 py-2 flex">
                            <x-button-enlace class="ml-auto mr-2 cursor-pointer" color="yellow"
                                wire:click="edit({{ $product_option->pivot->id }})" wire:loading.attr="disabled"
                                wire:target="edit({{ $product_option->pivot->id }})">Actualizar</x-button-enlace>
                            <x-button-enlace color="red" class="cursor-pointer" wire:click="$emit('deletePivot',{{ $product_option->pivot->id }} )">Eliminar</x-button-enlace>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @endif

    <x-jet-dialog-modal wire:model="open">
        <x-slot name="title">
            Editar Opciones
        </x-slot>
        <x-slot name="content">
            <div class="mb-4">
                <x-jet-label>
                    Opciones
                </x-jet-label>
                <select class="form-control w-full" wire:model="pivot_option_id">
                    <option value=" " selected disabled>Seleecione una opcion</option>
                    @foreach ($options as $option)
                        <option value="{{ $option->id }}">{{ ucfirst($option->name) }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <x-jet-label>
                    Cantidad
                </x-jet-label>
                <x-jet-input type="number" wire:model="pivot_quantity" placeholder="Ingrese una cantidad"
                    class="w-full" />
            </div>
            <div class="mt-4">
                <x-jet-label>
                    Precio
                </x-jet-label>
                <x-jet-input type="number" wire:model="pivot_price" placeholder="Ingrese un precio"
                    class="w-full" />
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-button-enlace color="red" class="cursor-pointer" wire:click="$set('open',false)">
                Cancelar
            </x-button-enlace>
            <x-button-enlace color="green" class="cursor-pointer" wire:click="update" wire:loading.attr="disabled"
                wire:target="update">
                Aceptar
            </x-button-enlace>
        </x-slot>
    </x-jet-dialog-modal>
</div>
