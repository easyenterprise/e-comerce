<div>
    <x-slot name="header">
        <div class="flex items-center">
            <h2 class="font-semibold text-xl text-gray-600 leading-tight">
                Productos
            </h2>
            <x-button-enlace class="ml-auto" color="green" href="{{ route('admin.products.create') }}">
                <i class="fas fa-plus-square mr-1"></i>Agregar Producto
            </x-button-enlace>
        </div>
    </x-slot>


    <div class="container my-4">
        <x-success-message />
    </div>
    {{-- Tabla de Productos --}}
    <div class="container py-2">
        <x-table-responsive>
            <div class="px-6 py-4 bg-gray-300">
                <x-jet-input class="w-full" type="text" wire:model="search"
                    placeholder="Ingrese el nombre del producto" />
            </div>

            @if ($products->count())
                <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-gray-50">
                        <tr>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Nombre
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Categoria
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Estado
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Precio
                            </th>
                            <th scope="col" class="relative px-6 py-3">
                                <span class="sr-only">Editar</span>
                            </th>
                            <th scope="col" class="relative px-6 py-3">
                                <span class="sr-only">Eliminar</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                        @foreach ($products as $product)
                            <tr>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="flex items-center">

                                        @isset($product->images->first()->url)
                                            <div class="flex-shrink-0 h-10 w-10">
                                                <img class="h-10 w-10 rounded-full object-cover" src="{{ Storage::url($product->images->first()->url) }}" alt="">
                                            </div>
                                            
                                        @else
                                            <div>
                                                <img class="h-12 w-12 rounded-full object-cover" src="{{asset('/storage/default/default-product.png')}}">
                                            </div>
                                        @endisset



                                        <div class="ml-4">
                                            <div class="text-sm font-medium text-gray-900">
                                                {{ $product->name }}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $product->subcategory->category->name }}
                                    </div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    @switch($product->status)
                                        @case(1)
                                            <span
                                                class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">
                                                Borrador
                                            </span>
                                            @break
                                        @case(2)
                                            <span
                                                class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                                Publicado
                                            </span>
                                            @break
                                        @default
                                            <span
                                                class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">
                                                Borrador
                                            </span>
                                    @endswitch
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm">
                                    @isset($product->price)
                                        <p class="text-gray-500">{{ $product->price }} MXN</p>
                                    @else
                                        <p class="text-red-500">Opciones D.</p>
                                    @endisset
                                </td>
                                <td class="whitespace-nowrap text-right text-sm font-medium">
                                    <div class="px-2 py-1 bg-yellow-400 rounded-lg text-center">
                                        <a href="{{ route('admin.products.edit', $product) }}"
                                            class="text-yellow-800 hover:text-white"><i class="fas fa-pencil-alt"></i> Editar</a>
                                    </div>
                                </td>
                                <td class="px-2 whitespace-nowrap text-right text-sm font-medium">
                                    <div class="px-2 py-1 bg-red-400 rounded-lg text-center">
                                        <a wire:click="$emit('deleteP', {{ $product->id }}  )" class="text-red-800 hover:text-white cursor-pointer"><i class="fas fa-trash"></i> Elimnar</a>
                                    </div>
                                </td>
                            </tr>

                        @endforeach

                        <!-- More people... -->
                    </tbody>
                </table>
            @else
                <div class="px-6 py-4 text-center bg-white">
                    No existe ningun registro coincidente
                </div>
            @endif
            @if ($products->hasPages())
                <div class="px-6 py-4 bg-gray-200">
                    {{ $products->links() }}
                </div>
            @endif

        </x-table-responsive>
    </div>

@push('script')
    <script>
        Livewire.on('deleteP', product => {
            Swal.fire({
                title: 'Estas seguro?',
                text: "Recuerda que esta accion no se puede revertir!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {

                    Livewire.emit('deleteProduct', product);

                    Swal.fire(
                        'Eliminado!',
                        'Se ha eliminado de la base de datos',
                        'success'
                    )
                }
            })
        })
    </script>
@endpush
</div>
