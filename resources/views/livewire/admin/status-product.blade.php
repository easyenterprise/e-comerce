<div class="bg-white shadow-lg rounded-lg p-6 mb-4">
    <p class="text-2xl text-center font-semibold mb-2">Estado del Producto</p>

    <div class="flex justify-around ">
        <label class="flex items-center justify-center p-2 rounded-lg shadow-lg bg-red-200 text-red-700 font-bold">
            <input wire:model.defer="status" type="radio" name="status" value="1">
            BORRADOR
        </label>
        <label class="flex items-center justify-center p-2 rounded-lg shadow-lg bg-green-200 text-green-700 font-bold">
            <input wire:model.defer="status" type="radio" name="status" value="2">
            PUBLICADO
        </label>

        <div class="flex justify-end items-center">
            <x-jet-action-message class="mr-3" on="saved">
                Actualizando...
            </x-jet-action-message>
            <x-button-enlace color="green" class="cursor-pointer" wire:click="save" wire:loading.attr="disabled" wire:target="save">
                Actualizar
            </x-button-enlace>
        </div>
    </div>
</div>
