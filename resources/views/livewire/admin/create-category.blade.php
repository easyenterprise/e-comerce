<div>
    <x-auth-validation-errors class="mb-4" />
    <x-jet-form-section submit="save" class="mb-6">
        <x-slot name="title">
            Crear nueva Categoria
        </x-slot>
        <x-slot name="description">
            Complete la informacion necesaria para crear una categoria
        </x-slot>

        <x-slot name="form">
            <div class="col-span-6 sm:col-span-4">
                <x-jet-label>
                    Nombre
                </x-jet-label>
                <x-jet-input wire:model="createForm.name" type="text" class="w-full mt-1" />
            </div>
            <div class="col-span-6 sm:col-span-4">
                <x-jet-label>
                    Slug
                </x-jet-label>
                <x-jet-input wire:model="createForm.slug" type="text" class="w-full mt-1 bg-gray-200" placeholder="Este campo depende del nombre" disabled />
            </div>
            <div class="col-span-6 sm:col-span-4">
                <x-jet-label>
                    Icono
                </x-jet-label>
                <x-jet-input wire:model.defer="createForm.icon" type="text" class="w-full mt-1" />
            </div>
            <div class="col-span-6 sm:col-span-6">
                <x-jet-label>
                    Marcas
                </x-jet-label>
                <div class="grid grid-cols-6">
                    @foreach ($brands as $brand)
                        <x-jet-label>
                            <x-jet-checkbox wire:model.defer="createForm.brands" name="brands[]" value="{{ $brand->id}}" />
                            {{ Str::limit($brand->name, 6) }}
                        </x-jet-label>
                    @endforeach
                </div>
            </div>

            <div class="col-span-6 sm:col-span-4">
                <x-jet-label>
                    Imagen
                </x-jet-label>
                <input wire:model="createForm.image" accept="image/*" type="file" class="mt-1" name="" id="{{$rand}}">
            </div>

        </x-slot>

        <x-slot name="actions">
            <x-jet-action-message class="mr-3" on="saved">
                Categoria creada!
            </x-jet-action-message>
            <x-button color="green">Agregar</x-button>
        </x-slot>
    </x-jet-form-section>

    <div class="container my-4">
        <x-success-message />
    </div>
    <x-jet-action-section>
        <x-slot name="title">
            Lista de Categorias
        </x-slot>
        <x-slot name="description">
            Aqui puede visualizar todas las categorias
        </x-slot>
        <x-slot name="content">
            <table class="text-gray-600">
                <thead class="border-b border-gray-300">
                    <tr class="text-left">
                        <th class="py-2 w-full">Nombre</th>
                        <th class="py-2 w-full">Accion</th>
                    </tr>
                </thead>
                <tbody class="divide-y divide-gray-300">
                    @foreach ($categories as $category)
                        <tr>
                            <td class="py-2">
                                <span class="inline-block w-8 text-center mr-2">
                                    {!!$category->icon!!}
                                </span>
                                <span class="uppercase">
                                    {{$category->name}}
                                </span>
                            </td>
                            <td class="py-2">
                                <div class="flex divide-x divide-gray-300">
                                    <div class="px-2 py-1 bg-yellow-400 rounded-lg text-center mr-1">
                                        <a href="#" class="text-yellow-800 hover:text-white text-xs font-semibold">Editar</a>
                                    </div>
                                    <div class="px-2 py-1 bg-red-400 rounded-lg text-center cursor-pointer">
                                        <a wire:click="$emit('deleteCategory', '{{$category->slug}}')" class="text-red-800 hover:text-white text-xs font-semibold">Eliminar</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </x-slot>
    </x-jet-action-section>
</div>
