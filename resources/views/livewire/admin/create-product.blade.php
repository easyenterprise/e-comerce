<div class="max-w-4xl mx-auto px-4 sm:px-6 lg:px-8 py-12 text-gray-800">
    <h1 class="text-3xl text-center font-semibold m-8">Agregar Producto</h1>
    <x-auth-validation-errors class="mb-4" />
    <div class="bg-white shadow-xl rounded-lg p-6"> 
        <div class="grid grid-cols-2 gap-6 mb-4">
            {{-- Categoria --}}
            <div>
                <x-jet-label value="Categorias" />
                <select class="w-full form-control" wire:model="category_id">
                    <option value="" selected disabled>Seleccione una categoría</option>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
    
            {{-- Subcategoria --}}
            <div>
                <x-jet-label value="Subcategorias" />
                <select class="w-full form-control" wire:model="subcategory_id">
                    <option value="" selected disabled>Seleccione una subcategoría</option>
                    @foreach ($subcategories as $subcategory)
                        <option value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    
        {{-- Nombre del producto --}}
        <div class="mb-4">
            <x-jet-label value="Nombre" />
            <x-jet-input type="text" wire:model="name" class="w-full" placeholder="Ingrese nombre del producto" />
        </div>
    
        {{-- Slug del producto --}}
        <div class="mb-4">
            <x-jet-label value="Slug" />
            <x-jet-input disabled type="text" wire:model="slug" class="w-full bg-gray-100" placeholder="Campo automatico" />
        </div>
    
        {{-- Descripcion --}}
        <div class="mb-4">
            <div wire:ignore>
                <x-jet-label value="Descripcion" />
                <textarea class="w-full form-control" rows="10" wire:model="description" x-data x-init="ClassicEditor
                .create( $refs.miEditor )
                .then(function(editor){
                    editor.model.document.on('change:data', () => {
                        @this.set('description', editor.getData())
                    })
                })
                .catch( error => {
                    console.error( error );
                } );" x-ref="miEditor"></textarea>
            </div>
        </div>
    
        <div class="mb-4 grid grid-cols-2 gap-6">
            {{-- Marcas --}}
            <div>
                <x-jet-label value="Marca" />
                <select class="form-control w-full" wire:model="brand_id">
                    <option value="" selected disabled>Seleccione una marca</option>
                    @foreach ($brands as $brand)
                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                    @endforeach
                </select>
            </div>
            @if ($subcategory_id)
            @if (!$this->subcategory->option)
            <div>
                <x-jet-label value="Precio" />
                <x-jet-input wire:model="price" type="number" class="w-full" step=".001" />
            </div>
            @endif
        @endif
            
        </div>
    
        @if ($subcategory_id)
            @if (!$this->subcategory->option)
                <div class="mb-4">
                    <x-jet-label value="Cantidad" />
                    <x-jet-input wire:model="quantity" type="number" class="w-full" />
                </div>
            @endif
        @endif
    
        <div class="flex">
            <x-button-enlace wire:click="save" wire:loading.attr="disabled" wire:target="save" class="ml-auto cursor-pointer" color="green">
                <i class="fas fa-save mr-1"></i>Agregar
            </x-button-enlace>
        </div>
    </div>
</div>
