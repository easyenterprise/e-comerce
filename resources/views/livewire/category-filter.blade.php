<div>
    <div class="bg-white rounded-lg shadow-lg mb-6">
        <div class="px-6 py-2 flex justify-between items-center">
            <h1 class="font-bold text-cyanColor2-800 uppercase">{{ $category->name }}</h1>
            <div
                class="grid grid-cols-2 border border-gray-200 rounded-lg divide-x divide-gray-200 text-gray-500 cursor-pointer">
                <i class="fas fa-border-all p-3 {{ $view == 'grid' ? 'text-skyColor1-500' : '' }}"
                    wire:click="$set('view', 'grid')"></i>
                <i class="fas fa-th-list p-3 {{ $view == 'list' ? 'text-skyColor1-500' : '' }}"
                    wire:click="$set('view', 'list')"></i>
            </div>
        </div>
    </div>

    <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-6">
        <aside>
            <h2 class="font-semibold text-center mb-2">Subcategorías</h2>
            <ul class="divide-y divide-gray-200">
                @foreach ($category->subcategories as $subcategory)
                    <li class="py-2">
                        <a class="cursor-pointer hover:text-skyColor1-700 capitalize {{ $subcategoria == $subcategory->name ? 'text-skyColor1-700 font-semibold' : '' }}"
                            wire:click="$set('subcategoria','{{ $subcategory->name }}')">{{ $subcategory->name }}</a>
                    </li>

                @endforeach
            </ul>
            <h2 class="font-semibold text-center mt-4 mb-2">Marcas</h2>
            <ul class="divide-y divide-gray-200">
                @foreach ($category->brands as $brand)
                    <li class="py-2">
                        <a class="cursor-pointer hover:text-skyColor1-700 capitalize {{ $marca == $brand->name ? 'text-skyColor1-700 font-semibold' : '' }}"
                            wire:click="$set('marca', '{{ $brand->name }}')">{{ $brand->name }}</a>
                    </li>

                @endforeach
            </ul>
            <x-jet-button class="mt-4" wire:click="limpiar">
                Eliminar Filtros
            </x-jet-button>
        </aside>
        <div class="md:col-span-2 lg:col-span-4">
            @if ($view == 'grid')
                <ul class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-6">
                    @foreach ($products as $product)
                        <li class="bg-white rounded-lg shadow ">
                            <a href="{{ route('products.show', $product) }}">
                                <article>
                                    <figure>
                                        <img class="h-48 w-full object-cover object-center rounded-t-md"
                                            src="{{ Storage::url($product->images->first()->url) }}" alt="" srcset="">
                                    </figure>
                                    <div class="py-4 px-6">
                                        <h1 class="text-lg font-semibold">
                                            {{ Str::limit($product->name, 15) }}
                                        </h1>
                                        @if ($product->price)
                                            <p class="font-bold text-indigo-900">${{ $product->price }} MXN</p>
                                        @else
                                            <p class="font-bold text-red-700">Opciones Disponibles</p>
                                        @endif
                                    </div>
                                </article>
                            </a>
                        </li>
                    @endforeach

                </ul>
            @else
                <ul>
                    @foreach ($products as $product)
                        <x-product-list :product="$product" />
                    @endforeach
                </ul>
            @endif
            <div class="mt-4">
                {{ $products->links() }}
            </div>
        </div>
    </div>
</div>
