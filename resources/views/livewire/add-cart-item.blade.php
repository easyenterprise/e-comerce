<div x-data>
    <p class="text-2xl font-semibold text-gray-700 my-2">$ {{ $product->price }} MXN</p>
    @role('Administrador|ClientePreferente')
    <div class="flex flex-col items-center justify-center rounded-md bg-greenLime-600 my-2 text-white">
        <p class="font-semibold">Acumulas {{ $product->points_sell }} puntos al comprar</p>
        <p>ó</p>
        <p class="font-semibold">Para comprar necesita {{ $product->points_buy }} puntos</p>
    </div>
    @endrole
    <div class="bg-white rounded-lg shadow-lg mb-6">
        <div class="p-4 flex items-center">
            <span class="flex items-center justify-center h-10 w-10 rounded-full bg-greenLime-600">
                <i class="fas fa-truck text-sm text-white"></i>
            </span>
            <div class="ml-4">
                <p class="text-lg font-semibold text-greenLime-600">Se hace envios a algunas areas locales
                </p>
                <p>Recibelo el {{ Date::now()->addDay(7)->format('l j F') }}</p>
            </div>
        </div>
    </div>

    <p class="text-gray-700 mb-4">
        <span class="font-semibold text-lg">Cantidad disponible: {{$quantity}}</span>
    </p>
    <div class="flex">
        <div class="mr-4">
            <x-jet-secondary-button 
                disabled
                x-bind:disabled="$wire.qty <= 1"
                wire:loading.attr="disabled"
                wire:target="decrement"
                wire:click="decrement">
                -
            </x-jet-secondary-button>

            <span class="mx-2 text-gray-700">{{ $qty }}</span>

            <x-jet-secondary-button 
                x-bind:disabled="$wire.qty >= $wire.quantity"
                wire:loading.attr="disabled"
                wire:target="increment"
                wire:click="increment">
                +
            </x-jet-secondary-button>
        </div>
        <div class="flex-1">
            <x-button color="blue" class="w-full"
            x-bind:disabled="$wire.qty > $wire.quantity"
            wire:click="addItem"
            wire:loading.attr="disabled"
            wire:target="addItem">
                Agregar a carrito de compras
            </x-button>
        </div>
    </div>
</div>
