<div class="container py-8">
    <section class="bg-indigo-50 rounded-lg shadow-lg p-6 text-gray-700">
        @if (Cart::count())
        <h1 class="text-lg font-semibold uppercase mb-6 border-b border-indigo-700 pb-2"><i class="fas fa-shopping-cart pr-2"></i>Carro de
            compras</h1>
        
        <table class="table-auto w-full">
            <thead>
                <tr>
                    <th></th>
                    @role('Administrador|ClientePreferente')
                    <th>Puntos</th>
                    @endrole
                    <th>Precio</th>
                    <th>Cantidad</th>
                    <th>Total</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach (Cart::content() as $item)
                {{-- ESTE BLOQUE REALIZA UN BUCLE DE LA VARIABLE DE SESION DEL COMPONENTE CART --}}
                    <tr>
                        <td>
                            <div class="flex">
                                <img class="h-15 w-20 object-cover mr-4 rounded" src="{{ $item->options->image }}"
                                    alt="">
                                <div>
                                    <p class="font-bold">{{ $item->name }}</p>
                                    @if ($item->options->option)
                                        <span class="mr-1">
                                            Opcion: {{ $item->options->option }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </td>
                        @role('Administrador|ClientePreferente')
                        <td class="text-center">
                            <p>{{$item->options['pointssell'] * $item->qty}}</p>
                        </td>
                        @endrole
                        <td><span>${{ $item->price }} MXN</span></td>
                        <td>
                            <div class="flex justify-center">
                                @if ($item->options->option)
                                    @livewire('update-cart-item-opcion', ['rowId' => $item->rowId], key($item->rowId))
                                @else
                                    @livewire('update-cart-item', ['rowId' => $item->rowId], key($item->rowId))
                                @endif
                            </div>
                        </td>
                        <td>
                            ${{ $item->price * $item->qty }}MXN
                        </td>
                        <td>
                            <a class="ml-6 cursor-pointer hover:text-red-600" 
                            wire:click="delete('{{$item->rowId}}')"
                            wire:loading.class="text-red-600 opacity-25"
                            wire:target="delete('{{$item->rowId}}')">
                                <li class="fas fa-trash"></li>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <a class="text-sm cursor-pointer hover:underline mt-8 inline-block hover:text-red-500" wire:click="destroy">
            <i class="fas fa-trash hover:text-red-500"></i> Borrar carrito de compras</a>
        @else
            <div class="flex flex-col items-center">
                <x-cart />
                <p class="text-lg text-gray-700 mt-4">TU CARRO DE COMPRAS ESTA VACIO</p>
                <x-button-enlace href="/" class="mt-4 px-16">
                    Ir a Comprar
                </x-button-enlace>
            </div>
        @endif
    </section>

    @if (Cart::count())
        <div class="bg-indigo-50 rounded-lg shadow-lg px-6 py-4 mt-4">
            <div class="flex justify-between items-center">
                <div>
                    <p class="text-gray-700">
                        <span class="font-bold text-lg">Total:</span>
                        $ {{Cart::subtotal()}} MXN
                    </p>
                </div>
                <div>
                    <x-button-enlace href="{{route('orders.create')}}">
                        Continuar
                    </x-button-enlace>
                </div>
            </div>
        </div>
    @endif
</div>
