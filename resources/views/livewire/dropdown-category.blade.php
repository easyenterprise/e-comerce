<div class="z-10">
    <x-dropdown-custom width="auto" align="top" dropdownClasses="" contentClasses="bg-yellow-400">

        <x-slot name="trigger">
            <div class="flex items-center justify-between w-72 cursor-pointer font-bold">
                <span class="mx-4 text-lg text-indigo-800">Categorías</span>
                <i class="fas fa-bars mx-4 text-indigo-800 text-lg"></i>
            </div>
        </x-slot>

        <x-slot name="content">
            <ul>
                @forelse ($categories as $category)
                <li class="text-indigo-900 hover:bg-indigo-900 hover:text-white">
                    <a href="{{ route('categories.show', $category)}}" class="py-2 px-4 text-sm flex items-center">
                        <span class="flex justify-center w-9">
                            {!! $category->icon !!}
                        </span>
                        <p class="capitalize">{{ $category->name }}</p>
                    </a>
                </li>
                @empty
                <li class="py-6 px-4">
                    <p class="text-center text-red-900 ">
                        No existen categorias 
                    </p>
                </li>
                @endforelse
            </ul>
        </x-slot>

    </x-dropdown-custom>
</div>