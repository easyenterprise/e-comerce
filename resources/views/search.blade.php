<x-app-layout>
    <div class="container py-8">
        <ul>
            @forelse ($products as $product)
                <x-product-list :product="$product" />
            @empty
            <li class="bg-white rounded-lg shadow-xl">
                <div class="flex items-center justify-center p-4">
                    <i class="fas fa-exclamation text-6xl text-red-600"></i>
                    <p class="text-6xl text-red-600 font-semibold">Ningun producto encontrado</p>
                </div>
            </li>
            @endforelse
        </ul>

        <div class="mt-4">
            {{$products->links()}}
        </div>
    </div>
</x-app-layout>
