<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <div class="mb-4 text-sm text-gray-600">
            {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didnt receive the email, we will gladly send you another.') }}
        </div>

        @if (session('status') == 'verification-link-sent')
            <div x-data="{ isVisible: true }" x-init="setTimeout(() => { isVisible = false; }, 10000)" x-show.transition.duration.1000ms="isVisible"
                class="bg-green-100 border-t border-r border-l border-b border-green-500 text-gren-700 px-4 py-3 rounded mb-2"
                role="alert">
                <p class="font-bold">Exito</p>
                <p class="text-sm"><i class="fas fa-exclamation-circle"></i> {{ __('A new verification link has been sent to the email address you provided during registration.') }}</p>
            </div>
        @endif

        <div class="mt-4 flex items-center justify-between">
            <form method="POST" action="{{ route('verification.send') }}">
                @csrf

                <div>
                    <x-jet-button type="submit">
                        {{ __('Resend Verification Email') }}
                    </x-jet-button>
                </div>
            </form>

            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <button type="submit" class="underline text-sm text-gray-600 hover:text-gray-900">
                    {{ __('Log Out') }}
                </button>
            </form>
        </div>
    </x-jet-authentication-card>
</x-guest-layout>
