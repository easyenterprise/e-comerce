<x-guest-layout>
    <div class="grid grid-cols-1 md:grid-cols-2">
        <div>
            <x-jet-authentication-card>
                <x-slot name="logo">
                    <x-jet-authentication-card-logo />
                </x-slot>
        
                <x-auth-validation-errors class="mb-4" />
        
                <form method="POST" action="{{ route('register') }}">
                    @csrf
        
                    <div>
                        <x-jet-label for="name" value="{{ __('Name') }}" />
                        <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
                    </div>
        
                    <div class="mt-4">
                        <x-jet-label for="email" value="{{ __('Email') }}" />
                        <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
                    </div>
        
                    <div class="mt-4">
                        <x-jet-label for="password" value="{{ __('Password') }}" />
                        <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
                    </div>
        
                    <div class="mt-4">
                        <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                        <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
                    </div>
        
                    <div class="mt-4">
                        <label for="">CLiente Preferente</label>
                        <input type="radio" name="rolechek" checked value="1">
                    </div>
        
                    <div class="mt-4">
                        <label for="">CLiente Normal</label>
                        <input type="radio" name="rolechek" value="2">
                    </div>
        
                    @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                        <div class="mt-4">
                            <x-jet-label for="terms">
                                <div class="flex items-center">
                                    <x-jet-checkbox name="terms" id="terms"/>
        
                                    <div class="ml-2">
                                        {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                                'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Terms of Service').'</a>',
                                                'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Privacy Policy').'</a>',
                                        ]) !!}
                                    </div>
                                </div>
                            </x-jet-label>
                        </div>
                    @endif
        
                    <div class="flex items-center justify-end mt-4">
                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                            {{ __('Already registered?') }}
                        </a>
        
                        <x-jet-button class="ml-4">
                            {{ __('Register') }}
                        </x-jet-button>
                    </div>
                </form>
        
                
            </x-jet-authentication-card>
        </div>
        <div class="bg-gradient-to-tr from-blue-400 to-purple-500">
            <div class="container">
                <div class="text-white mt-24 flex flex-col items-center">
                    <h1 class="text-4xl">Cliente Preferente</h1>
                    <p class="mt-4 text-justify">Registrate y paga para obtener tu suscripcion anual por tan solo <span class="font-bold text-indigo-100">$300 pesos</span>, y empieza a disfrutar de todos los beneficios que tenemos para ti!</p>
                </div>
                <div class="grid grid-cols-1 my-8 lg:grid-cols-2 gap-4 lg:gap-6">
                    <input type="text" placeholder="Nombre" class="px-4 py-2 rounded-lg border border-indigo-500 font-bold text-indigo-600 placeholder-indigo-400 focus:outline-none focus:ring-2 focus:ring-indigo-200">
                    <input type="email" placeholder="Correo" class="px-4 py-2 rounded-lg border border-indigo-500 font-bold text-indigo-600 placeholder-indigo-400 focus:outline-none focus:ring-2 focus:ring-indigo-200">
                    <input type="phone" placeholder="Telefono" class="px-4 py-2 rounded-lg border border-indigo-500 font-bold text-indigo-600 placeholder-indigo-400 focus:outline-none focus:ring-2 focus:ring-indigo-200">
                    <input type="text" placeholder="CURP" class="px-4 py-2 rounded-lg border border-indigo-500 font-bold text-indigo-600 placeholder-indigo-400 focus:outline-none focus:ring-2 focus:ring-indigo-200">
                    <input type="password" placeholder="Contraseña" class="px-4 py-2 rounded-lg border border-indigo-500 font-bold text-indigo-600 placeholder-indigo-400 focus:outline-none focus:ring-2 focus:ring-indigo-200">
                    <input type="password" placeholder="Confirmacion de contraseña" class="px-4 py-2 rounded-lg border border-indigo-500 font-bold text-indigo-600 placeholder-indigo-400 focus:outline-none focus:ring-2 focus:ring-indigo-200">
                </div>
                <div>
                    @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                    <div class="mt-4">
                        <x-jet-label for="terms">
                            <div class="flex justify-end">
                                <x-jet-checkbox name="terms" id="terms"/>
    
                                <div class="ml-2 text-white">
                                    {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                            'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-white hover:text-blue-300">'.__('Terms of Service').'</a>',
                                            'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-white hover:text-blue-300">'.__('Privacy Policy').'</a>',
                                    ]) !!}
                                </div>
                            </div>
                        </x-jet-label>
                    </div>
                @endif
                </div>
                <div class="my-8 flex justify-end">
                    <button class=" bg-white px-4 py-2 font-bold text-indigo-700 rounded-lg" type="submit">Registrarse && Pagar</button>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
